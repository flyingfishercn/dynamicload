const {
    Device,
} = __dyn

//import {
//    isString,
//    isArray,
//    isDictionary,
//    defaultString
//} from './utils.js'


// TODO: 支持调用 native api 获取屏幕宽高
const SCREEN_WIDTH = Device.getScreenWidth()

const vm = {
    // jumpUrl: '',
    data: ({originData}) => {
        let data = originData

        let mainVideoId = originData.mainVideoId
        let mainVideoImg = originData.mainVideoImg
        let playUrl = originData.playUrl

        let isHasMainVideo = true
        if(mainVideoId=="" || mainVideoId==undefined){
          isHasMainVideo= false
        }
        // 用来存储转换后的数据
        let imageListData = []
        if (isHasMainVideo) { // 判断插入视频帧数据
          // type：0标识是图片视图，type:1标识是视频视图，
          let mainVideoData = {"mainVideoId":mainVideoId,"index":0,"mainVideoImg":mainVideoImg,"playUrl":playUrl,"type":"1"}
          imageListData[0] = mainVideoData
        }
        // 遍历数组获取组装成map形式
        originData.imgList.forEach(function(item,index){
        if (isHasMainVideo) { // 如果有视频，则此处序号需要加1作为索引
           ++index
        }
           let newItem = {"url":item,"index":index,"type":"0"}
           imageListData[index] = newItem

        })
        data['imgList']= imageListData
        data['imgListLength']= imageListData.length
//        data['jsName'] = "js代码ss1"
        return data
    }
}

__dyn.render(vm)
