class Node{
    type:number;
    symbol:string;
    value:any;
    constructor(type, symbol, value){
        this.type = type;       //INFIX_OP
        this.symbol = symbol;   //'index_op'
        this.value = value;     //+
    }
}

export class stack_jd{
    tokens:Node[]
    size:number
    constructor() {
        this.tokens = [];
        this.size = 0;
    }

    stackPush(type, symbol, value){
        const node = new Node(type, symbol, value)
        this.tokens.push(node);
        this.size++;
    }

    stack_pop(){
        let node =this.tokens.pop()
        this.size--;
        return node;
    }

    stack_top(){
        let node = this.tokens[this.size - 1]
        return node
    }

    stack_is_empty(){
        return (this.size) === 0;
    }
    
    stack_print_all(){
        let result = "";
        console.log("stack size =", this.tokens.length);
        for(let i =0; i<this.size; i++){
            result = result + this.tokens[i].value
            console.log("this.tokens ["+this.tokens[i].type + ", "+this.tokens[i].symbol+", "+this.tokens[i].value+"]");
        }
        return result
    }
}