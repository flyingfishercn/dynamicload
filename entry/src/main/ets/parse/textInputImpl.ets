import { transAttrsValue } from '../common/utils/index';
import { ViewNode } from '../viewmodel/viewNode'
import logger from '../common/Logger'
import hilog from '@ohos.hilog';
import {getProperties, parseValue} from './tools'

function getTextContent(viewNode: ViewNode, external?: any): string {
  hilog.fatal(0x0001, "jddebug", "index.ets %{public}s", JSON.stringify(globalThis.jsonProduct))
  let text = viewNode?.attributes?.['text']
  if((text?.search(/\${.*}/)) != -1 && external) {
    let property_list = getProperties(text)
    if(property_list.length > 0) {
      text = ""
      for(let i = 0; i<property_list.length; i++) {
        if (i>0) {
          text += "--";
        }
        text += external[property_list[i]] ? String(external[property_list[i]]): property_list[i] + " not defined!"
      }
    }
  }
  logger(`getTextContent ${text}`)
  
  //text = tokenizeCaculate("1+2+3")
  return text
}

@Builder
export function TextInputImplParse(viewNode: ViewNode, external?: any): void  {
  TextArea({ placeholder: viewNode && viewNode.attributes? viewNode.attributes['placeholder']: '', text: getTextContent(viewNode,external), controller:new TextInputController()})
  .commonAttrsParse_TextInput(viewNode)
  .specifiedParse(viewNode)
}

interface EnterKeyTypeMap {
  [index: string]: EnterKeyType
}

const enterKeyTypeMap: EnterKeyTypeMap = {
    "actionUnspecified": EnterKeyType.Done,
    "actionSearch": EnterKeyType.Search,
    "actionDone": EnterKeyType.Done,
    "actionGo": EnterKeyType.Go,
    "actionNext": EnterKeyType.Next,
    "actionSend": EnterKeyType.Send,
}

interface AlignMap {
  [index: string]: TextAlign
}

const alignMap: AlignMap = {
    "center": TextAlign.Center,
    "left": TextAlign.Start,
    "right": TextAlign.End,
}

@Extend(TextArea)
function gravity_textInput(value: string) {
    .textAlign(alignMap[value])
}

@Extend(TextArea)
function hintColor_textInput(value: ResourceColor) {
    .placeholderColor(value)
}

interface WeightMap {
  [index: string]: FontWeight
}

const weightMap: WeightMap = {
    "bold": FontWeight.Bold,
    "normal": FontWeight.Normal
}

@Extend(TextArea)
function textStyle_textInput(value: string) {
    .fontWeight(weightMap[value])
}

@Extend(TextArea)
function textSize_textInput(value: string) {
    .fontSize(Number(value))
}

@Extend(TextArea)
function textColor_textInput(value: ResourceColor) {
    .fontColor(value)
}

@Extend(TextArea)
function padding_textInput(value:Padding) {
  .padding(value)
}
@Extend(Scroll)
function margin_textInput(value: Margin) {
    .margin(value)
}

@Extend(TextArea)
function backgroundColor_textInput(value: string) {
  .backgroundColor(value)
}

@Extend(TextArea)
function width_textInput(value: string) {
  .width(transAttrsValue(value))
}

@Extend(TextArea)
function size_textInput(value: SizeOptions) {
  .size(value)
}

@Extend(TextArea)
function height_textInput(value: string) {
  .height(transAttrsValue(value))
}

@Extend(TextArea)
function border_textInput(value: BorderOptions) {
  .border(value)
}

@Extend(TextArea)
function onclick_textInput(value: string) {
  .onClick((event: ClickEvent)=>{})
}

@Extend(TextArea)
function commonAttrsParse_TextInput(viewNode: ViewNode) {
  if (viewNode && viewNode.attributes) {
    let margin:Margin = {
      left:0,right:0,top:0,bottom:0
    };
    let padding:Padding = {
      left:0,right:0,top:0,bottom:0
    };
    for (let [key, value] of Object.entries(viewNode.attributes)) {
      switch(key) {
        case 'hintColor':
            $.hintColor_textInput(value)
            break;
        case 'textStyle':
            $.textStyle_textInput(value)
            break;
        case 'textSize':
            $.textSize_textInput(value)
            break;
        case 'textColor':
            $.textColor_textInput(value)
            break;
        case 'layoutId':
            break;
        case 'padding':
            $.padding_textInput(JSON.parse(value))
            break;
        case 'paddingLeft':
            padding.left = value;
            $.padding_textInput(padding)
            break;
        case 'paddingRight':
            padding.right = value;
            $.padding_textInput(padding)
            break;
        case 'paddingTop':
            padding.top = value;
            $.padding_textInput(padding)
            break;
        case 'paddingBottom':
            padding.bottom = value;
            $.padding_textInput(padding)
            break;
        case 'marginRight':
            margin.right = value;
            $.margin_textInput(margin)
          break;
        case 'marginLeft':
            margin.left = value;
            $.margin_textInput(margin)
          break;
        case 'marginTop':
            margin.top = value;
            $.margin_textInput(margin)
          break;
        case 'marginBottom':
            margin.bottom = value;
            $.margin_textInput(margin)
          break;
        case 'backgroundColor':
        case 'bgColor':
            $.backgroundColor_textInput(value);
            break;
        case 'width':
            $.width_textInput(value);
            break;
        case 'height':
            $.height_textInput(value);
            break;
        case 'border':
            break;
        case 'size':
            break;
        case 'onclick':
            $.onclick_textInput(value);
            break;
        case 'gravity':
            $.gravity_textInput(value);
            break;
        default:
      }
    }
  }
}

@Extend(TextArea)
function specifiedParse(viewNode: ViewNode) {
  if (viewNode && viewNode.attributes) {
    for (let [key, value] of Object.entries(viewNode.attributes)) {
      switch(key) {
        case 'placeholderColor':
            $.placeholderColor_textInput(value);
            break;
        case 'placeholderFont':
            $.placeholderFont_textInput(JSON.parse(value));
            break;
        case 'enterKeyType':
            break;
        case 'caretColor':
            break;
        case 'maxLength':
            break;
        case 'inputFilter':
            break;
        case 'copyOption':
            break;
        default:
            break;
      }
    }
  }
}

interface InputTypeMap {
  [index: string]: InputType
}

const inputTypeMap: InputTypeMap = {
    "Number": InputType.Number,
    "Normal": InputType.Normal,
    "Email": InputType.Email,
    "Password": InputType.Password,
}

@Extend(TextArea)
function placeholderColor_textInput(value: ResourceColor) {
  .placeholderColor(value)
}

@Extend(TextArea)
function placeholderFont_textInput(value: Font) {
  .placeholderFont(value)
}

@Extend(TextArea)
function enterKeyType_textInput(value: BorderOptions) {

}

@Extend(TextArea)
function caretColor_textInput(value: BorderOptions) {

}

@Extend(TextArea)
function maxLength_textInput(value: BorderOptions) {

}

@Extend(TextArea)
function inputFilter_textInput(value: BorderOptions) {

}

@Extend(TextArea)
function copyOption_textInput(value: BorderOptions) {

}