import {tokenizeCacluate} from '../../parser_jd/eval_javascript_parse';
import hilog from '@ohos.hilog';

export const transAttrsValue = (value) => {

    let result = tokenizeCacluate(value, "");

    let res = ((result.search(/%/) != -1) || typeof result === 'object' ? result : Number(result))
    hilog.fatal(0x0001, "width-height-debug", "index.ets %{public}s caculate result %{public}d res %{public}d", value, result, res);

    return res
}