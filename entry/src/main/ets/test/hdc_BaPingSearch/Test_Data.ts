export let Search_data = {
    "HcCid3": "655",
    "isFood": false,
    "MergeSKUType": "mergesku_not_hit_cate",
    "gaiaContent": true,
    "adEventIdSecond": "0",
    "requerySearch": "0",
    "articleInserted": "1",
    "IsSpecialStock": "0",
    "netContent": true,
    "HcCid1": "9987",
    "HcCid2": "653",
    "promType": "icon_id",
    "adEventId": "7",
    "topInfoType": "searchHeaderConfig",
    "UserPersonalization": "262144",
    "promotionImgUrl": "https://m.360buyimg.com/umm/jfs/t1/184331/34/27360/2083/62e8fbcaEea0d61cc/2c0df9bbfb0f49de.png",
    "realCount": "10",
    "showStyleRule": "mate50_655_1_list",
    "secondInserted": "1",
    "searchHeaderConfig": {
        "floorContent": [
            {
                "identifier": "LM1",
                "content": [
                    {
                        "picWidth": "1125",
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/12272/8/19127/255829/63171a76Ed7955d84/2b67cc6f2088eb94.jpg",
                        "picHeight": "1530",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/12272/8/19127/255829/63171a76Ed7955d84/2b67cc6f2088eb94.jpg",
                        "skuId": "100037199897"
                    }
                ]
            },
            {
                "identifier": "LM2",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/120921/4/29717/19363/63171215E58ea4aeb/b352574c12efb2c9.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/120921/4/29717/19363/63171215E58ea4aeb/b352574c12efb2c9.jpg"
                    }
                ]
            },
            {
                "rotationTime": "3000",
                "identifier": "LM7",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/175348/12/29606/101579/631712ebEdbb7c3af/ca3b6b29e2849f2e.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/175348/12/29606/101579/631712ebEdbb7c3af/ca3b6b29e2849f2e.jpg",
                        "skuId": "100037199897"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/36199/28/17566/103997/631712f9Ea34031ba/96473cad44658a4a.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/36199/28/17566/103997/631712f9Ea34031ba/96473cad44658a4a.jpg",
                        "skuId": "100035295081"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/110738/20/30010/104868/63171336E4777bb53/9c09cc306af85e2d.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/110738/20/30010/104868/63171336E4777bb53/9c09cc306af85e2d.jpg",
                        "skuId": "100037199889"
                    }
                ]
            },
            {
                "identifier": "LM2",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/130613/32/26242/28855/63171395E5e5d5c55/67d5209b31f38ab9.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/130613/32/26242/28855/63171395E5e5d5c55/67d5209b31f38ab9.jpg"
                    }
                ]
            },
            {
                "identifier": "LM10",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/187758/14/28017/38665/631713aeEd251da07/7c348d3a77b83afa.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/187758/14/28017/38665/631713aeEd251da07/7c348d3a77b83afa.jpg",
                        "jumpUrl": "https://pro.m.jd.com/mall/active/4Rmq6KQikoTksgBSs5CntFatEs8j/index.html"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/44666/27/21963/38316/631713bbEa2113f1a/426082b7d615944b.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/44666/27/21963/38316/631713bbEa2113f1a/426082b7d615944b.jpg",
                        "jumpUrl": "https://lzkj-isv.isvjcloud.com/lzclient/e5c6d4e29d2a47ef8666a420ec38103f/cjwx/common/entry.html?activityId=e5c6d4e29d2a47ef8666a420ec38103f&amp;gameType=wxTurnTable"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/171235/33/29875/32857/631713c1E9a7e99ad/80a729616de6d8dc.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/171235/33/29875/32857/631713c1E9a7e99ad/80a729616de6d8dc.jpg",
                        "jumpUrl": "https://lzkj-isv.isvjcloud.com/drawCenter/activity/61b81b7281304375b7d8dbeecce9b5c9?activityId=61b81b7281304375b7d8dbeecce9b5c9"
                    }
                ]
            },
            {
                "identifier": "LM2",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/108590/4/29994/24385/63171422Ebad4e5d1/b436e1f826fcb4ab.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/108590/4/29994/24385/63171422Ebad4e5d1/b436e1f826fcb4ab.jpg"
                    }
                ]
            },
            {
                "identifier": "LM5",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/22334/31/19083/167055/6317145cEedb5c425/4ae2e449363421e6.png",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/22334/31/19083/167055/6317145cEedb5c425/4ae2e449363421e6.png"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/100104/4/31015/120470/63171461Ed2d7347f/cada60e756ea58d9.png",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/100104/4/31015/120470/63171461Ed2d7347f/cada60e756ea58d9.png"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/184583/14/28574/267797/6317146aE09fc8a9c/8c5278624ac1b7d3.png",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/184583/14/28574/267797/6317146aE09fc8a9c/8c5278624ac1b7d3.png"
                    }
                ]
            },
            {
                "identifier": "LM2",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/98242/2/30729/19618/6317147dE6c2c6d39/2a6ac87f4d8ab12a.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/98242/2/30729/19618/6317147dE6c2c6d39/2a6ac87f4d8ab12a.jpg"
                    }
                ]
            },
            {
                "rotationTime": "3000",
                "identifier": "LM7",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/198330/18/27168/482283/631ae215E28e3bbed/d9dbda048bc2fe44.png",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/198330/18/27168/482283/631ae215E28e3bbed/d9dbda048bc2fe44.png",
                        "skuId": "100037397249"
                    }
                ]
            },
            {
                "identifier": "LM9",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/185114/13/27477/80652/631714bcE78ad936f/aa4bb7d763faec90.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/185114/13/27477/80652/631714bcE78ad936f/aa4bb7d763faec90.jpg",
                        "skuId": "100037503770"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/110741/17/29815/81954/631714ceE2d5b5886/f1dddb0c603c64eb.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/110741/17/29815/81954/631714ceE2d5b5886/f1dddb0c603c64eb.jpg",
                        "skuId": "100030711508"
                    }
                ]
            },
            {
                "identifier": "LM9",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/15871/2/18037/68162/631714e2E94663f39/cebd2a9c0ad2d4d2.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/15871/2/18037/68162/631714e2E94663f39/cebd2a9c0ad2d4d2.jpg",
                        "skuId": "100026819200"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/136398/39/28613/73236/631714f3Ec8977270/372de8ab86be0b91.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/136398/39/28613/73236/631714f3Ec8977270/372de8ab86be0b91.jpg",
                        "skuId": "100027028257"
                    }
                ]
            },
            {
                "identifier": "LM9",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/177158/27/28646/77543/63171504E5fc9834d/dc72594ad00bcada.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/177158/27/28646/77543/63171504E5fc9834d/dc72594ad00bcada.jpg",
                        "skuId": "100016944073"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/142986/22/29192/79115/63171515Ec415993f/2461f454f2d4ed48.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/142986/22/29192/79115/63171515Ec415993f/2461f454f2d4ed48.jpg",
                        "skuId": "100027028259"
                    }
                ]
            },
            {
                "identifier": "LM9",
                "content": [
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/108381/24/32464/80230/63171524Ee6b4fd9e/04596e840894a77d.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/108381/24/32464/80230/63171524Ee6b4fd9e/04596e840894a77d.jpg",
                        "skuId": "100024857657"
                    },
                    {
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/54164/25/20636/75831/6317152eE07a76562/fe67962833e3c3e0.jpg",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/54164/25/20636/75831/6317152eE07a76562/fe67962833e3c3e0.jpg",
                        "skuId": "100032253085"
                    }
                ]
            },
            {
                "identifier": "LM1",
                "content": [
                    {
                        "picWidth": "1125",
                        "hxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/15494/11/18284/10561/63171550Edec2c968/47cb67e5f520f14c.jpg",
                        "picHeight": "148",
                        "lxImg": "https://m.360buyimg.com/mobilecms/jfs/t1/15494/11/18284/10561/63171550Edec2c968/47cb67e5f520f14c.jpg",
                        "jumpUrl": "https://pro.m.jd.com/mall/active/3QxGBykRhcABgSuQo5yJ6sxVogMb/index.html"
                    }
                ]
            }
        ],
        "animationTime": "1000",
        "activityCode": "2186",
        "backgroundColor": "#FFFFFF",
        "darkBackgroundColor": "#FFFFFF",
        "uet": {
            "touchstone_expids": "tsabtest|base64|UGNBQlRlc3RfMTU5Mzl8ZXhw|tsabtest"
        },
        "activityName": "审-2华为预售霸屏",
        "headerType": "07",
        "topImgColor": "1"
    }

}