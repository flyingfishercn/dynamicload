export let dsl_update_event =
    [
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '201',
                'text': "Dynamic Text Changed-1",
                'fontSize': '15',
                'textColor': '#000000',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#A0A0EE',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '202',
                'text': "Dynamic Text Changed-2",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FFCCCC',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '203',
                'text': "Dynamic Text Changed-3",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FFCC66',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '204',
                'text': "Dynamic Text Changed-4",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FFCC00',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '205',
                'text': "Dynamic Text Changed-5",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FFCC99',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '206',
                'text': "Dynamic Text Changed-6",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FF9999',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '207',
                'text': "Dynamic Text Changed-7",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FF9966',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '208',
                'text': "Dynamic Text Changed-8",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#999933',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '209',
                'text': "Dynamic Text Changed-9",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#99AA77',
            },
        },
        {
            id: "updateTextAttr",
            params : {
                'layoutId': '210',
                'text': "Dynamic Text Changed-10",
                'fontSize': '15',
                'textColor': '#585897',
                'width': '80%',
                'height': '40',
                'backgroundColor': '#FF6666',
            },
        },
    ]

export let dsl_data = {
    "selfPickSites": [
        {
            identifier: 1,
            layoutId: 201,
            text: "为您推荐以下自提点",
            siteName: "如家快捷酒店门廊北侧丰巢柜"
        },
        {
            siteName: "国航总部大楼",
            layoutId: 202,
            showMessage: "地址：北京市北京市顺义区天柱路30号 ",
            address: "地址:北京市北京市顺义区天柱路30号还不错吧导航报错敦煌壁画吧导航报错敦煌壁画吧导航报错敦煌壁画",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59",
            "unavailableMsg": "测试的不可用文案提示文案提示文案提示文案提示"
        },
        {
            siteName: "国家会计学院东门丰巢2号柜",
            layoutId: 203,
            address: "地址:北京北京市顺义区空港街道丽苑街9号国家会计学院东门内侧",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "吉祥花园小区丰巢快递柜",
            layoutId: 204,
            showMessage: "地址：北京市顺义区裕华路空港经济开发区B区吉祥花园 ",
            address: "地址:北京市顺义区裕华路空港经济开发区B区吉祥花园"
        },
        {
            siteName: "水木兰亭三期乙21号楼丰巢柜",
            layoutId: 205,
            showMessage: "地址：北京北京市顺义区天竺镇府前一街58号水木兰亭三期乙21号楼西侧 ",
            address: "地址:北京北京市顺义区天竺镇府前一街58号水木兰亭三期乙21号楼西侧"
        },
        {
            siteName: "天裕昕园小区20号楼北侧丰巢柜",
            layoutId: 206,
            showMessage: "地址：北京市北京市顺义区天北路 ",
            address: "地址:北京市北京市顺义区天北路"
        },
        {
            siteName: "北京市天竺房地产公司9号楼西北门速递易柜2号柜",
            layoutId: 207,
            showMessage: "地址：北京市北京市顺义区裕丰路 ",
            address: "地址:北京市北京市顺义区裕丰路",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "航信产业园东门丰巢3号柜",
            layoutId: 208,
            showMessage: "地址：北京北京市顺义区天北路东50米中国航信高科技产业园东门 ",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区天北路东50米中国航信高科技产业园东门",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "金地悦景台西门内侧丰巢柜1号柜",
            layoutId: 209,
            showMessage: "地址：北京市北京市顺义区裕庆路16号院 ",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "国航信息大楼大门西侧丰巢柜",
            layoutId: 210,
            showMessage: "地址：北京市北京市顺义区天柱西路16号 ",
            helpMessage: "[详细地图]",
            address: "地址:北京市北京市顺义区天柱西路16号",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "香沁园1号楼南侧丰巢柜",
            layoutId: 211,
            showMessage: "地址：北京北京市顺义区火沙路香沁园1号楼南侧宣传栏旁 ",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区火沙路香沁园1号楼南侧宣传栏旁",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "天竺花园配电室东侧丰巢柜",
            layoutId: 212,
            showMessage: "地址：北京北京市顺义区府前一街38号天竺花园 ",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区府前一街38号天竺花园",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "中关村医学工程健康产业化基地北门丰巢柜",
            layoutId: 213,
            showMessage: "地址：北京市北京市顺义区安祥大街10号 ",
            address: "地址:北京市北京市顺义区安祥大街10号",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "天竺房地产西门传达室丰巢柜",
            layoutId: 214,
            showMessage: "地址：北京北京市顺义区裕丰路天竺房地产开发公司西门传达室北侧 ",
            address: "地址:北京北京市顺义区裕丰路天竺房地产开发公司西门传达室北侧",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "如家快捷酒店门廊北侧丰巢柜",
            layoutId: 215,
            showMessage: "地址：北京北京市顺义区府前一街32号如家快捷酒店 ",
            address: "地址:北京北京市顺义区府前一街32号如家快捷酒店",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "国航总部大楼",
            layoutId: 216,
            showMessage: "地址：北京市北京市顺义区天柱路30号 ",
            address: "地址:北京市北京市顺义区天柱路30号还不错吧导航报错敦煌壁画吧导航报错敦煌壁画吧导航报错敦煌壁画",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59",
            "unavailableMsg": "测试的不可用文案提示文案提示文案提示文案提示"
        },
        {
            siteName: "国家会计学院东门丰巢2号柜",
            layoutId: 217,
            address: "地址:北京北京市顺义区空港街道丽苑街9号国家会计学院东门内侧",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "吉祥花园小区丰巢快递柜",
            layoutId: 218,
            showMessage: "地址：北京市顺义区裕华路空港经济开发区B区吉祥花园 ",
            address: "地址:北京市顺义区裕华路空港经济开发区B区吉祥花园"
        },
        {
            siteName: "水木兰亭三期乙21号楼丰巢柜",
            layoutId: 219,
            showMessage: "地址：北京北京市顺义区天竺镇府前一街58号水木兰亭三期乙21号楼西侧 ",
            address: "地址:北京北京市顺义区天竺镇府前一街58号水木兰亭三期乙21号楼西侧"
        },
        {
            siteName: "天裕昕园小区20号楼北侧丰巢柜",
            layoutId: 220,
            showMessage: "地址：北京市北京市顺义区天北路 ",
            address: "地址:北京市北京市顺义区天北路"
        },
        {
            siteName: "北京市天竺房地产公司9号楼西北门速递易柜2号柜",
            layoutId: 221,
            showMessage: "地址：北京市北京市顺义区裕丰路 ",
            address: "地址:北京市北京市顺义区裕丰路",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "航信产业园东门丰巢3号柜",
            layoutId: 222,
            showMessage: "地址：北京北京市顺义区天北路东50米中国航信高科技产业园东门 ",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区天北路东50米中国航信高科技产业园东门",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "金地悦景台西门内侧丰巢柜1号柜",
            layoutId: 223,
            showMessage: "地址：北京市北京市顺义区裕庆路16号院 ",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "国航信息大楼大门西侧丰巢柜",
            layoutId: 224,
            showMessage: "地址：北京市北京市顺义区天柱西路16号 ",
            helpMessage: "[详细地图]",
            address: "地址:北京市北京市顺义区天柱西路16号",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "香沁园1号楼南侧丰巢柜",
            layoutId: 225,
            showMessage: "地址：北京北京市顺义区火沙路香沁园1号楼南侧宣传栏旁",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区火沙路香沁园1号楼南侧宣传栏旁",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "天竺花园配电室东侧丰巢柜",
            layoutId: 226,
            showMessage: "地址：北京北京市顺义区府前一街38号天竺花园",
            helpMessage: "[详细地图]",
            address: "地址:北京北京市顺义区府前一街38号天竺花园",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "中关村医学工程健康产业化基地北门丰巢柜",
            layoutId: 227,
            showMessage: "地址：北京市北京市顺义区安祥大街10号",
            address: "地址:北京市北京市顺义区安祥大街10号",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "天竺房地产西门传达室丰巢柜",
            layoutId: 228,
            showMessage: "地址：北京北京市顺义区裕丰路天竺房地产开发公司西门传达室北侧",
            address: "地址:北京北京市顺义区裕丰路天竺房地产开发公司西门传达室北侧",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        },
        {
            siteName: "如家快捷酒店门廊北侧丰巢柜",
            layoutId: 229,
            showMessage: "地址：北京北京市顺义区府前一街32号如家快捷酒店",
            address: "地址:北京北京市顺义区府前一街32号如家快捷酒店",
            businessHoursStart: "00:00",
            businessHoursEnd: "23:59"
        }
    ]
}

export const miniPdCollectData = {
    "wareInfoList": [
        {
            "isShowsameShop":true,
            "identifier":1,
            "itemType": 0,
            "rt": 0,
            "wareId": "100020407638",
            "wname": "箭牌（ARROW）卫浴抽水马桶大冲力 家用连体虹吸坐便器 节水卫生间坐厕座便器 大口径排污AE1185-AM",
            "markType": 0,
            "isMonetized": false,
            "imageurl": "https://m.360buyimg.com/mobilecms/s714x714_jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg!q70.dpg",
            "imageurlType": 0,
            "good": "",
            "commentCount": "",
            "jdPrice": "1029.00",
            "isSamWare": false,
            "isPlusWare": false,
            "isPinGouWare": false,
            "isFansWare": false,
            "book": "false",
            "promotion": "false",
            "commentAbStrategy": "A",
            "mp": 0,
            "feminine": false,
            "extension_id": "{\"ad\":\"\",\"ch\":\"\",\"shop\":\"\",\"sku\":\"\",\"ts\":\"\",\"uniqid\":\"{\\\"pos_id\\\":\\\"5652\\\",\\\"sid\\\":\\\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\\\",\\\"sku_id\\\":\\\"100020407638\\\"}\"}",
            "samePicPid": "0-12508-1-20-1-4848",
            "opPrice": "1199.00",
            "interactive": "0",
            "client_exposal_url": "https://sh.jd.com/d?fl=AgXpeamhHFvsOKOIx72oO7IE9M_ouuaBGWV9uSqt6DAsDyhsYN_AgbSTw9OucqreLO7GgXzVU2gHJGA5xLhsuYtIdNtOWBa2fvGn_UjSUBffw0VFdIgWJiqZcwL9EaNbdwn86Gf70UDZytR6r1sBDHZ1ZAjAXS8DD7I_18JzSI9IhOqSKc72RK1qnVawAdT_wWHyAy7zS4JLiNor-LL0ug",
            "client_click_url": "https://ccc-x.jd.com/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC8xMDAwMjA0MDc2MzguaHRtbA&log=Sw-g3NLr0A9TThjD1x6arzsAJU6X-DawwD85qfz5BQzXl8rUCwxS-GOMzpyFCJ4k5i7HSzKZEGHpwglCsjk02xwdQvZxnpSHLyIh0qYwoYw5WZ8HiIHVAJRqt3iomNz5Lca9C2tCGWF355zNNdC2J1SvOQLBWWS41k7_Q2K43VAHQjJitYbhJNn3st3QZronzKXNRK85IEO1aZPbkP6ZGfZUdo5EHJ8uTNfIsvK0cAZ4L3Yq-z31O8nxH-iBc3LTtuxMgL0s_2IrtFVqeCX2fN-byRLmpKlt0MaboO3-iatsfQLN3YGWHX2zrSSGTuZm1mU7goGKAiBZvVwsaPQcung5L78nAt7KgGzvTK-nuSUdn7K69ee8X_Yhpi-xi1EgGtJ279Xrgs0_JuLibtAzZvTJqLDbsnsXnnuL0PlGdUvelfk-I-D9aYYI56mdy2-n1GDOVwIYKw9EAcE72oGQMxPLqtqHagj6IgBfhP_biJYpxnzUiFT5GJlRQJE38yyL2hiSPRDziTJp476za53mPrx-z_0dmjIVtc1eifXd4FavnKa_T7JCHnvN_evprMrhdYy3_MooPkr_T6Zy889yrvPSeCQO0-0IpA-JuEc0OK7Lds9M0Jze2U3l1DIfytruYvxMztjOClTDrWS3EUSaR6nrQkjlu4Kgbd5U8ORThf7XsdBUSy2r7DpYAlcvxpqvDrt6Y0wunSnJkv92PGLbRrAtru7Ga9Pt7Lk6vcpxV1KU573046b-37IAUJKE4gz6ZVuH1qKffxZ8XqcMyWbYZ-kZ_aaYWhNLzz33BIkat8ifeFS8zcOljb3CxmC3f19fVj-Q4NlNw_K7rFcPwLK9NLmhqTw1uQYwprA5we6u8QxyqFculvIsb5Bcf5TbjFH3onoRYXeYNHPKehg6_vmIw-vuXIjNqX6OQXT7KsgwkloRxlkEonbGmuiW0quCsNGQQumlZn2Hmlz_kDX-N8tlv-vYnnqN963PTagTfcJ9tVrrxauIenFCcyyXMs3vB_SXMaVbul2eCBwNS3sl8kCWWeg4kD6mp7l87gfT0CfK94eAQvRSr99NGcGxbpWAqW9gB-zpDB1QNpp6DIgELPpSKFGILVI66Ur2-U8eIBQ4SEksCRq3ebjSy44ky0jVtH7Z6ygrt3OciRZfjCh_TxSGi_V85HNN6e_KfPr1-q0Vj_FA-skR0aqoZj4873-9k9U3Wi3CCOATSVGtdc-VlV7uhrZk1BTOqfXCNI9VrTsnfZ1SDvCCSaWSiOPd7scKig38OBgbLkwyLdFl3u-jCmrkvRp0xXOF-pEWWmzR7gjfEsrBJEvRNAh7jQviNwcOPXfQARiKzHxQLIKF6s6c1LxPCOzhvjTptC4y5rUPlTP96j6bvl8yq9HH9rll0vk3gYSf8z2wVrQCGdel_6Bc2ID_qHL-y2CHkR3507--lohNUN-ya5Sx32B1mvZvxW62jP381oLOKushhLNkHzdoHDZqgwuwTbFRsedlppTgHG2RmNYkmSGUy6unYEXL3y_zusTRKlAYoWlGZNJcPbnb2OcFL46cetPUHInQSFB1qOiKFHs8cbwjoAHxMjS4KciQqJjFyura7A76u1wR07AVwiIFFH-XWHMy1M5uPhODsdtRxWo&v=404&clicktype=1&",
            "canClipTitleImg": true,
            "playUrl": "https://jvod.300hu.com/vod/product/800cbb7d-4580-47a9-a7e4-ecfd9f4562de/0bb4d0073f6b49b0b5b52085c51b5f8c.mp4",
            "duration": 39,
            "exposureJsonSourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDIwNDA3NjM4LCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"静音设计,去除污渍,全管道施釉\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"1\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"5\",\"index\":\"0\",\"liveId\":\"11131498\",\"imp_style\":\"2\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000368187\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"100020407638\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "recomReasonStyle": "1",
            "freeShippingFlag": "0",
            "liveSkuStatus": "5",
            "goodsType": 1,
            "promotionType": 1,
            "p": "710033",
            "isArrivalPriceSupportStoreGoods": "0",
            "isArrivalPriceBelt": "1",
            "beltType": "1",
            "purchasePrice": "859",
            "totalBenefit": "170",
            "miniCommentCount": "2万+",
            "isFollow": false,
            "arrivePriceCode": "tab_213",
            "arrivePriceText": "到手价",
            "arrivePriceColor": "#FA2C19",
            "isMiniWareDetailArrivePrice": "1",
            "brokerInfo": "eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDIwNDA3NjM4LCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==",
            "impStyle": "2",
            "shopLogoSquare": "https://img30.360buyimg.com/popshop/jfs/t1/157752/28/16641/21335/6066c6cdE35e6e2d4/7c04b3db6cf614b0.png",
            "sameShopNum": "1",
            "miniInteractive": "0",
            "purchaseNum": "1",
            "flow": "101",
            "adword": "",
            "startRemainTime": 0,
            "endRemainTime": 0,
            "sid": "c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd",
            "isFeedBackSlide": 0,
            "followCount": "",
            "category1": "9855",
            "category2": "9857",
            "category3": "9910",
            "stockStateId": 33,
            "remainNum": -1,
            "wareHouseNum": "2",
            "clickUrl": "http://ccc-x.jd.local/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC8xMDAwMjA0MDc2MzguaHRtbA&log=Sw-g3NLr0A9TThjD1x6arzsAJU6X-DawwD85qfz5BQzXl8rUCwxS-GOMzpyFCJ4k5i7HSzKZEGHpwglCsjk02xwdQvZxnpSHLyIh0qYwoYw5WZ8HiIHVAJRqt3iomNz5Lca9C2tCGWF355zNNdC2J1SvOQLBWWS41k7_Q2K43VAHQjJitYbhJNn3st3QZronzKXNRK85IEO1aZPbkP6ZGfZUdo5EHJ8uTNfIsvK0cAZ4L3Yq-z31O8nxH-iBc3LTtuxMgL0s_2IrtFVqeCX2fN-byRLmpKlt0MaboO3-iatsfQLN3YGWHX2zrSSGTuZm1mU7goGKAiBZvVwsaPQcung5L78nAt7KgGzvTK-nuSUdn7K69ee8X_Yhpi-xi1EgGtJ279Xrgs0_JuLibtAzZvTJqLDbsnsXnnuL0PlGdUvelfk-I-D9aYYI56mdy2-n1GDOVwIYKw9EAcE72oGQMxPLqtqHagj6IgBfhP_biJYpxnzUiFT5GJlRQJE38yyL2hiSPRDziTJp476za53mPrx-z_0dmjIVtc1eifXd4FavnKa_T7JCHnvN_evprMrhdYy3_MooPkr_T6Zy889yrvPSeCQO0-0IpA-JuEc0OK7Lds9M0Jze2U3l1DIfytruYvxMztjOClTDrWS3EUSaR6nrQkjlu4Kgbd5U8ORThf7XsdBUSy2r7DpYAlcvxpqvDrt6Y0wunSnJkv92PGLbRrAtru7Ga9Pt7Lk6vcpxV1KU573046b-37IAUJKE4gz6ZVuH1qKffxZ8XqcMyWbYZ-kZ_aaYWhNLzz33BIkat8ifeFS8zcOljb3CxmC3f19fVj-Q4NlNw_K7rFcPwLK9NLmhqTw1uQYwprA5we6u8QxyqFculvIsb5Bcf5TbjFH3onoRYXeYNHPKehg6_vmIw-vuXIjNqX6OQXT7KsgwkloRxlkEonbGmuiW0quCsNGQQumlZn2Hmlz_kDX-N8tlv-vYnnqN963PTagTfcJ9tVrrxauIenFCcyyXMs3vB_SXMaVbul2eCBwNS3sl8kCWWeg4kD6mp7l87gfT0CfK94eAQvRSr99NGcGxbpWAqW9gB-zpDB1QNpp6DIgELPpSKFGILVI66Ur2-U8eIBQ4SEksCRq3ebjSy44ky0jVtH7Z6ygrt3OciRZfjCh_TxSGi_V85HNN6e_KfPr1-q0Vj_FA-skR0aqoZj4873-9k9U3Wi3CCOATSVGtdc-VlV7uhrZk1BTOqfXCNI9VrTsnfZ29IZf-GorsZCCP5QIMx1vHLxUXp0hfd0jmCU2Nc1ReJ3BHgKJVFi9WKLZlTg7fn9jUIiqjFQaaWAhkBc6R8mUtK5HbahOdKMY5axC8bJTfVKdGyUOfDiuHfdizKrYl45RI0LSNWsasQ6fUePHsim2ex0OLRkCoKUO7bsEoGSef_2iz3y7ip2q5HQQaWyF1CH3N4ADtEHRV52IJDIMIim9JoxwWSblRPpr13EF6mUSVc_L0jG0FHlpeGSML5DEoGxyY8cSw7VBDRght6s_1hvSXmJ6XGozmEFRcecEp2UUKZAFqoBIGqtAEt710OnyGZNJdDjOh7xBCIrvybV7_qzYA7WYLXEvXMKhIg7BFNMJBKUkmg4MEwq1Rz-zhAMMNQ7E&v=404&clicktype=1&",
            "similarEnter": "1",
            "canAddCart": "1",
            "subWareList": [
                {
                    "wareId": "100025709586",
                    "imageUrl": "https://m.360buyimg.com/mobilecms/s714x714_jfs/t1/49835/40/21013/123211/6314a8f8E235842ce/4fdd673c39c96498.jpg!q70.dpg",
                    "wareTitle": "箭牌(ARROW)卫浴抽水马桶家用 虹吸连体坐便器小尺寸普通座便 小户型卫生间坐厕 微晶釉面 1级水效AE1126L",
                    "sourceValue": "{\"shopid\":\"1000368187\",\"mainsku\":\"100020407638\",\"sameShopNum\":\"1\",\"skuid\":\"100025709586\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\"}",
                    "exposureJsonSourceValue": "{\"shopid\":\"1000368187\",\"mainsku\":\"100020407638\",\"sameShopNum\":\"1\",\"skuid\":\"100025709586\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\"}",
                    "jdPrice": "969.00",
                    "client_exposal_url": "https://sh.jd.com/d?fl=qxlvxJOs4rNo9cGqUUQBkAp-nfPmfioj1H_qxXhGbI0HoCSOx9SpHkrYysT0M_Iobtjjqy3b-PjIxDT1eAKeAn7NidR2vKEFlB5739U4d4JCfb7voey6ljX9O2-Vg2CTnLL6bY9p8B45dsJjCWsN1cKhquXNfmoW6bl3tW2Vaq2ZuGCOPfVP_np6_HBnMSFg",
                    "extension_id": "{\"ad\":\"\",\"ch\":\"\",\"shop\":\"\",\"sku\":\"\",\"ts\":\"\",\"uniqid\":\"{\\\"pos_id\\\":\\\"5652\\\",\\\"sid\\\":\\\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\\\",\\\"sku_id\\\":\\\"100025709586\\\"}\"}",
                    "client_click_url": "https://ccc-x.jd.com/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC8xMDAwMjU3MDk1ODYuaHRtbA&log=Sw-g3NLr0A9TThjD1x6arzsAJU6X-DawwD85qfz5BQzXl8rUCwxS-GOMzpyFCJ4k5i7HSzKZEGHpwglCsjk028oH--0HwogtKLKXQhSv7qvrfpBUO9rmFx2G7VjA5VpbxWBJPiTjR28LRmrcyr6j3UU_V9Uz8lYhMlxeWvS-VuRPCFKuVH6VXigb5idh0zlYrhvm8BSzBcsdQPUYiujvVj2D-1jpu4dGcXsbFTdv8rwurdEC85GYXW9sGqvCueEw-obv3CbM9YWgCWRBtkyreK4niAlDUEuCrvmlSoOW40HE_SX3EJxMcne2valBa6azncKWWZ8hPoUx-ArpnWg_c2yUTdhpg2zAeXTw-_Ie0tk-R35z8bodO1KRdgA0qbjEBy-anz2wvfeuEt7dO527nBm8Ck7shpm9FNhJArDMc4WinXqOw-4twJEFNAnNbHmRY0PN_OZ5E-DJpDAh9lej7mzfG20llxRmXcHrSx5ewwivyuvH0ZbbNCcArhupyJmGrX94WBYw_QJJXbLHxQHfyN0AbvI_oRDh2lXpQzyCR5pQHPru1Iot40G6W717lwO3-QH4LdRwc4eEORl-v5Z3yI1Fna2hiBfFcobdp5tPR7wS4WSx2NifKTA0caLbFzsKfPgKqvLJBLDZykghVxwT43WeBSPwa9S3DKILyXTBIbcfGKDAiungoox2TM0hLbP0sUhf9EtnkujQY4QiaRaVc_S6lQ_LlWWx7lsHkzm23KUE6Iqi7-m9Uhq7ppjuoq9L2NI1jmSQtcX0VV96tTFjx_KXdETdW22GckepfzM19ss5MMdPxYrEne_383oPzckBl3E_JonUNb1nn31WAkcVoGyKlabCiZxOBc9nn60-9DU_Fu6eiOilpOmkFc61shVn6xHYaEIJYkSgJZnVirkOldGgahcFSNyNi77xAryFck8N-QB_24dtuytJxwssz85tGf_H6fOeWsCRoYo45S9yI7k7lhvJq_cThzqkeLALV8jS6xuGn14o1FOm4had27Gu9nUMjHNq-3JpEjWagJbL4VpuAxFjvH3bfmu92UkOJ-ohILx4BdeW89h8YlGyGMAvqNBf1FJNtqoPqXX3o6KDiY4_ip-MDns8qheZYWNEvGayoEESiBuyhh7Jo0BS43VxeRDgaG1uMuuEZQ-MbNGfaHTrTzobANbdoIH-miF9o3JETwAp9FewBVyMZtFh82HiI8WHvS8GinmPIH7_Da2RA_1rLMEIus6ZUB43zE4-wD3FFzR9lxnrb5qYRHw3vAdnW2P9zoLFzDv30RUWJ6MHSzlwTgLHsy4bub0zvW9WSqpi4I0pBtIVH0Uvf0C2uOPxzXQZvfRENXN_eOd_lyTzwfCnP950bpIqN2YeBD5mogz39Gj_rf2cLcmu5NF_Rv9nG3PPG7oudg7k2qZVoftibuVGlGbJ6gN-ixM-QLr4udkIuJqfPYjf_KaRVhD5Q0rFPDTdDPWeeDBsxgQukhj8F9eN8WlMRYE0k5evwWKkTMTnhpVMe5a4BgghS8HcSOvkrXI8Xy3jJTxUF1GzqIxOgqG4Hx7gWVikvQ0CFTLbSaXvHWNZiZ4yVSbN5Tqo3RDqOisRITu3iQbmJVutQFfMtFvb2KJfsivdd4wXzAOPj1agHrtJCIWxp5_xIkvgXO5w&v=404&clicktype=1&",
                    "source": "",
                    "rt": "0"
                }
            ],
            "couponSortType": 0,
            "canNegFeedback": "1",
            "reqsig": "85bcd60a3a615902b25362ea214dcaa798b791a0",
            "abt": "-1",
            "isDotScheme": false,
            "presaleWare": 0,
            "renewal": 0,
            "icon2": "tab_232",
            "tipsMapList": [],
            "venderId": "1000368187",
            "sourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDIwNDA3NjM4LCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"静音设计,去除污渍,全管道施釉\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"1\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"5\",\"index\":\"0\",\"liveId\":\"11131498\",\"imp_style\":\"2\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000368187\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"100020407638\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "shopSourceValue": "{\"shopid\":\"1000368187\",\"mainsku\":\"100020407638\",\"sameShopNum\":\"1\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\"}",
            "sourceValueFeedback": "{\"itemid\":\"-100\",\"cvgsku\":\"-100\",\"index\":\"0\",\"page\":\"1\",\"source\":\"1\",\"type\":\"-100\",\"skuid\":\"100020407638\",\"flow\":\"101\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"matrt\":\"-100\"}",
            "sourceValueSimilar": "{\"p\":\"710033\",\"index\":\"0\",\"page\":\"1\",\"source\":\"1\",\"skuid\":\"100020407638\",\"flow\":\"101\",\"sid\":\"c69cfaf6-0e71-489b-a91d-1a6d3e01f0dd\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "source": "1",
            "expid": "",
            "feedBackReason": [
                {
                    "name": "不感兴趣",
                    "icon": "feedback_0",
                    "id": 0
                },
                {
                    "name": "商品不够新颖",
                    "icon": "feedback_18",
                    "id": 18
                },
                {
                    "name": "品类不喜欢",
                    "icon": "feedback_1",
                    "id": 1
                },
                {
                    "name": "已经买了",
                    "icon": "feedback_5",
                    "id": 5
                },
                {
                    "name": "商品图引起不适",
                    "icon": "feedback_6",
                    "id": 6
                },
                {
                    "name": "涉及隐私",
                    "icon": "feedback_7",
                    "id": 7
                }
            ],
            "feedBackStrategy": "B",
            "isCoupon": "0",
            "isFlashPurchase": "1",
            "mainVideoId": "593527515",
            "mainVideoImg": "https://jvod.300hu.com/img/2021/86739697/1/img2.jpg",
            "openShopJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000368187\",\"venderId\":\"1000368187\",\"jumpTab\":\"home\"}",
            "miniCanAddCart": "1",
            "sellPointNewFlag": "1",
            "sellPoint": [
                "静音设计",
                "去除污渍",
                "全管道施釉"
            ],
            "shopName": "箭牌卫浴京东自营官方旗舰店",
            "shopId": "1000368187",
            "imgList": [
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/43496/15/19132/186209/6314ae8aE5d56d54d/9460e81d406eb10a.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/145333/38/29171/176336/6314ae9cE62ed8948/a7fe8be62eb6583a.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/191128/17/12254/63631/60e6cb35E33fd9d79/b6e159548a850817.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/172599/1/18829/74033/60e6cb35E66c9f7b3/5828461183ae21b6.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/180683/32/13203/122397/60e6cb35Ee1ffa474/46d5040865b65f04.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/182383/10/13514/98107/60e9090bEeef3d9c3/3910d54787271e2b.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/211675/23/15710/277175/61dfc601E052a0a1c/72e42760bbb7a446.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/189731/36/12016/86462/60e6cb35E3fdf23e7/e0663527ba804e41.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/223582/23/9499/118259/62689eceEce3df280/1399105d4e9fbed5.jpg!q70.dpg"
            ],
            "wareBusiness": {
                "code": 0,
                "floors": [
                    {
                        "bId": "eCustom_flo_614",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "topOn": false,
                            "isOpenCar": true,
                            "supportSale": true,
                            "soldOversea": {
                                "isSoldOversea": true,
                                "soldOverseaService": {
                                    "soldOverseaIcon": "https://m.360buyimg.com/cc/jfs/t4984/195/1172610074/2110/e12abb06/58ede3e4Nfc650507.png",
                                    "soldOverseaText": "售全球",
                                    "soldOverseaDesc": "支持收货地址为海外或港澳台地区"
                                },
                                "soldOverseaStr": "7"
                            },
                            "abTest800": true,
                            "yuyueInfo": {
                                "isYuYue": false,
                                "isbuyTime": false,
                                "mad": false,
                                "plusType": 0,
                                "showDraw": false,
                                "yuYue": false,
                                "yuyueNum": 0
                            },
                            "isOpenNode": true,
                            "property": {
                                "isJzfp": false,
                                "newReceiveCoupon": true,
                                "suitABTest": 0,
                                "buyMaxNum": 200,
                                "isJnbtNewText": false,
                                "plus20Flag": false,
                                "isOverseaPurchase": "0",
                                "easyBuy": true,
                                "isFxyl": false,
                                "isRegisterUser": false,
                                "isFlimPrint": "",
                                "isShowShopNameB": false,
                                "dpgRevisionFlag": true,
                                "showEmptyPrice": false,
                                "threeSuperFlag": false,
                                "noStockOrder": false,
                                "virtualCardUrl": "https://gamerecg.m.jd.com?skuId=100020407638&chargeType=9910&skuName=箭牌（ARROW）卫浴抽水马桶大冲力 家用连体虹吸坐便器 节水卫生间坐厕座便器 大口径排污AE1185-AM&skuPrice=1029.00",
                                "isfans": true,
                                "batchReceiveCoupon": true,
                                "addAndSubToast": {
                                    "lowestToastText": "最少购买1件哦！"
                                },
                                "isEasyBuyPrice": true,
                                "isRx": false,
                                "sdkDegrade": false,
                                "stockNotice": false,
                                "opForIos": true,
                                "isCollect": false,
                                "recTabEnable": true,
                                "isJx": false,
                                "isOTC": "0",
                                "cartFlag": true,
                                "wareImageTest": "",
                                "brandId": "9149",
                                "isOP": false,
                                "isPop": false,
                                "androidImageSwitch": true,
                                "shareUrl": "https://item.m.jd.com/product/100020407638.html",
                                "chatUrl": "https://m.360buyimg.com/n3/jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg",
                                "evaluateTabEnable": false,
                                "category": "9855;9857;9910",
                                "isRegularPrice": true,
                                "isEncrypt": true
                            },
                            "otherUseBannerInfo": {
                                "flashDegrade": false,
                                "bannerText": "使用优惠预估价",
                                "koDegrade": false,
                                "bannerPrice": "¥859.00",
                                "businessEnum": "1"
                            },
                            "leVieuxFusil": false,
                            "newStyle": false,
                            "priceLabel": "¥",
                            "abTestInfo": {
                                "addCarRecommendAB": false,
                                "askMedicineInfoAb": false,
                                "attentionAB": "A",
                                "collectABInfo": {
                                    "bottom3ABTest": true,
                                    "bottom4ABTest": true,
                                    "titleABTest": true
                                },
                                "easyDelAB": true,
                                "evaluateAB": false,
                                "feedBackAB": true,
                                "hasChangeButton": true,
                                "hospitalAB": "A",
                                "hwShare": false,
                                "newUser": false,
                                "newuserFreeAb": false,
                                "noBotmShop": false,
                                "packABTest": 1,
                                "qaUpAb": false,
                                "rankYhTag": true,
                                "recommendPopup": false,
                                "recommendYhTag": true,
                                "shareM": "a",
                                "shareShield": false,
                                "shopABTest": "a",
                                "shopCardTypeAb": true,
                                "shopExtendsAb": true,
                                "shopIntensifyAB": true,
                                "shopPromotionAB": "B",
                                "show3cService": true,
                                "showBuyLayer": true,
                                "skuSource": 2,
                                "specialSelectAB": true,
                                "toABTest": true
                            },
                            "shopInfo": {
                                "customerService": {
                                    "hasChat": false,
                                    "hasJimi": false,
                                    "allGoodJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000368187\",\"venderId\":\"1000368187\",\"jumpTab\":\"allProduct\"}",
                                    "mLink": "https://m.jd.com/product/100020407638.html",
                                    "online": false,
                                    "inShopLookJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000368187\",\"venderId\":\"1000368187\",\"jumpTab\":\"home\"}"
                                }
                            },
                            "flashInfo": {
                                "banner": "https://m.360buyimg.com/mobilecms/jfs/t3079/270/5085309438/2440/77dba8ec/5861cc06N113d618f.png",
                                "cd": 98512,
                                "cdBanner": "https://m.360buyimg.com/mobilecms/jfs/t3208/230/5077057080/2463/853a9aa/5861ccb7N731e030d.png",
                                "desc": "距闪购结束还剩:",
                                "endTime": 1662566400000,
                                "icon": "detail_028",
                                "promoPrice": "1029",
                                "startTime": 1662465600000,
                                "state": 2
                            },
                            "isDesCbc": true,
                            "isNewPP": true,
                            "isOpen": true,
                            "toHandssSrengthen": {
                                "buttonShow": true,
                                "selectShow": true,
                                "predictText": "预估到手价",
                                "toHandsPrice": "¥859",
                                "toHandsText2Yugu": true,
                                "toHandsText": "到手价"
                            },
                            "jumpToProduct": false,
                            "jdwlType": 0,
                            "eventParam": {
                                "sep": "{\"area\":\"1_2800_55837_0\",\"sku\":[[\"100020407638\",\"1029.00\",\"现货,明日19:00前下单，预计(09月08日)送达\",\"33\",\"0\"]]}"
                            },
                            "priceInfo": {
                                "rangePriceFlag": false,
                                "jprice": "1029.00"
                            },
                            "showAttentionButton": true,
                            "tenthRevision": false,
                            "attentionInfo": {
                                "txt2": "已收藏",
                                "txt1": "收藏"
                            },
                            "weightInfo": {
                                "content": "不计重量",
                                "title": "重量"
                            },
                            "buttonInfo": {
                                "main": {
                                    "bgColor": "#FFA600,#FFB000,#FFBC00;#E59500,#E59B00,#E6A800;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "立即购买",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 1
                                },
                                "second": {
                                    "bgColor": "#F10000,#FF2000,#FF4F18;#D80000,#E51C00,#E54615;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "加入购物车",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 0
                                }
                            },
                            "bottomButtonInfo": {
                                "leftButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 1,
                                    "buttonHightColor": "#D80000,#E51C00,#E54615",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#F10000,#FF2000,#FF4F18",
                                    "buttonPriority": 240,
                                    "buttonText": "加入购物车",
                                    "buttonTextColor": "#ffffffff"
                                },
                                "rightButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 2,
                                    "buttonHightColor": "#E59500,#E59B00,#E6A800",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#FFA600,#FFB000,#FFBC00",
                                    "buttonPriority": 240,
                                    "buttonSubText": "到手价¥859",
                                    "buttonText": "立即购买",
                                    "buttonTextColor": "#ffffffff"
                                }
                            },
                            "pointInfo": {
                                "isShowAr": false,
                                "isOPType": 0
                            },
                            "wareImage": [
                                {
                                    "small": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg!q70.dpg.webp",
                                    "big": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg!q70.dpg.webp",
                                    "share": "https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/213369/30/21219/124073/6314ae78E1b8677e2/990aa2656b5947f1.jpg!q70.jpg"
                                }
                            ],
                            "oldMoreStyle": false,
                            "wareInfo": {
                                "name": "箭牌（ARROW）卫浴抽水马桶大冲力 家用连体虹吸坐便器 节水卫生间坐厕座便器 大口径排污AE1185-AM",
                                "venderId": "1000368187",
                                "skuId": "100020407638"
                            },
                            "promotionInfo": {
                                "isBargain": false,
                                "prompt": ""
                            },
                            "tabUrl": "https://in.m.jd.com/product/graphext_0_0_0_0_0/100020407638.html",
                            "isOpenH5": true,
                            "isScf": false,
                            "locCouponFlag": false,
                            "generalTrackDic": {
                                "shopId": "1000368187",
                                "skuId": "100020407638"
                            },
                            "ddFatigueMechanism": {
                                "leftBottomBubbleCount": "5",
                                "leftBottomBubbleDuration": "5",
                                "ddBottomToastDuration": "10",
                                "ddBottomToastCount": "5",
                                "ddRightCount": "5"
                            },
                            "report": {
                                "darkIcon": "https://m.360buyimg.com/cc/jfs/t1/207978/19/1794/611/614c4aabE2b82675b/68307d0266c3aab7.png",
                                "icon": "https://m.360buyimg.com/cc/jfs/t1/199632/26/9352/400/614c4a83E7615a396/11f6a5f4c1eb8497.png",
                                "isOpen": true,
                                "jumpUrl": "https://h5.m.jd.com/babelDiy/Zeus/2WH8HhtMB5TQ2KcjAbrrW3atvo68/index.html#/home?pin=jd_61fdcba78e4b7&sku=100020407638",
                                "title": "我要反馈"
                            },
                            "appointAndPresaleOpen": true,
                            "plusShieldLandedPriceFlag": false,
                            "shareImgInfo": {
                                "priceDes": "价格具有时效性",
                                "promotionStr": "",
                                "markingOff": true,
                                "shareLanguage": "推荐一个好物给你，请查收",
                                "secondPrice": "1199.00",
                                "jprice": "1029.00",
                                "priceUrl": "https://m.360buyimg.com/cc/jfs/t1/32239/11/7019/5065/5c933b3dE4a485f97/f3a371d0407ef626.png"
                            }
                        },
                        "mId": "miniMasterdata",
                        "sortId": 1
                    },
                    {
                        "bId": "eCustom_flo_615",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "preferentialGuide": {
                                "secondLevelGuideInfo": {
                                    "sLevelGuideText1": "使用以下“*”标优惠，预估价¥859.00 ",
                                    "sLevelIconCode": "https://m.360buyimg.com/cc/jfs/t1/75639/12/11713/385/5d91bd73E9865f7db/4122771acdea2dc7.png",
                                    "sLevelGuideText2": "以上优惠仅为初步预估，不代表最终价格，规则可在帮助中心查看",
                                    "sLevelJumpUrl": "https://ihelp.jd.com/l/help/scene/getSceneDetail?id=322666",
                                    "saveMoney": "¥859.00",
                                    "sLevelCode": "*"
                                },
                                "couponInfo": [],
                                "iconCode": "detail_var_045",
                                "firstLevelGuideInfo": {
                                    "fLevelGuideText": "使用优惠预估价¥859.00 ",
                                    "saveMoney": "¥859.00",
                                    "fLevelCode": "detail_047"
                                },
                                "businessEnum": "1",
                                "promotion": {
                                    "activity": [],
                                    "activityTypes": [
                                        "15"
                                    ],
                                    "attach": [],
                                    "bestProList": [
                                        {
                                            "value": "每满96元，可减17元现金，最多可减1258元",
                                            "text": "满减",
                                            "skuId": "",
                                            "proSortNum": 40,
                                            "link": "",
                                            "proId": "200372293539",
                                            "promoId": "200372293539"
                                        }
                                    ],
                                    "canReturnHaggleInfo": false,
                                    "gift": [],
                                    "isBargain": false,
                                    "isTwoLine": true,
                                    "limitBuyInfo": {
                                        "noSaleFlag": "0",
                                        "limitNum": "0",
                                        "strategyFlag": "0",
                                        "isPlusLimit": "0",
                                        "limitUserFlag": "1",
                                        "limitAreaFlag": "1",
                                        "limitPeopleFlag": "1"
                                    },
                                    "normalMark": "tab_var_071",
                                    "plusDiscountMap": {},
                                    "plusMark": "tab_var_124",
                                    "prompt": "",
                                    "screenLiPurMap": {},
                                    "tip": "",
                                    "tips": []
                                },
                                "hasFinanceCoupon": false
                            }
                        },
                        "mId": "miniAggrePromo",
                        "sortId": 2
                    }
                ],
                "others": {
                    "templateType": "mini"
                },
                "wait": 0
            },
            "jdShop": false
        },
        {
            "itemType": 0,
            "rt": 0,
            "wareId": "7725561",
            "wname": "恒洁 大冲力节水防臭抗污马桶171 5年质保 PP塑料 上按两段式",
            "markType": 0,
            "isMonetized": false,
            "imageurl": "https://m.360buyimg.com/mobilecms/s714x714_jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg!q70.dpg",
            "imageurlType": 0,
            "good": "",
            "commentCount": "",
            "jdPrice": "999.00",
            "isSamWare": false,
            "isPlusWare": false,
            "isPinGouWare": false,
            "isFansWare": false,
            "book": "false",
            "promotion": "false",
            "commentAbStrategy": "A",
            "mp": 0,
            "feminine": false,
            "extension_id": "{\"ad\":\"5651\",\"ch\":\"2\",\"sku\":\"7725561\",\"ts\":\"1662467887\",\"uniqid\":\"{\\\"material_id\\\":\\\"5413267573\\\",\\\"pos_id\\\":\\\"5651\\\",\\\"sid\\\":\\\"b360ad54-e2dc-4492-9a41-ab9b54976c89\\\"}\"}",
            "samePicPid": "0-12508-0-142-0-38065",
            "opPrice": "1399.00",
            "interactive": "0",
            "client_exposal_url": "https://sh.jd.com/d?fl=pMGeb_2s__iGVJsW3ui62jMynogfGd7N7CI5ZqAe5etvHkreO3qRNnkDiXM_8atEQ8aAyhwQYKQmohsTpjzawwg7aabpBwMMrnZVu9gQGJgWpblyVUujhTI13SYe5t7RgJkOYDHena1fnW1MwXLvw6cxOpX4q5oOBhnA4smZMlbLRutxwBxRW38sY-O-U6Ly",
            "client_click_url": "https://ccc-x.jd.com/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC83NzI1NTYxLmh0bWw&log=lX_ecXwcWJG5wH87RGkIpBgF1sW1Rig-k28Si60HsEie-xZYWa91f802L2v-on4is7XX3J0HVt-K4A7EpxYViQVjiYLqHoDw7-DFp1X_eMPXVbh8fEN2B4Z-r84uux110ov5NoVF54uWH0siPDo1GrjYzNN97D7pz2JdGATRLwz5FOoWihZhJCe2ffBj1XP40X9DfTgqcXhtU6-9Q88V2bS3EpdIGG_PHAEaTiCvWufe5DX2noEnFAV-LS5nAGo05XW525PlMDx_d9yfXqnqLRvY5wNOrntN1EkGmqSLMSBGPsDlkd1xgsZwze_HIGiF49yBVgjDp5E0tbHfEAQB8WH1rarxISvJLNqQGGcwnzQbtnYSlICg7jrxTT1Cy2YwcRjdxKnPzgmn4AQoUO3ynhgHYmVEKy12JmOz8HzVQWP2adpNs2SJzqhW8smd-7bCAQMZ3_38Q3aPKqHKvcbhMbiezC-do3K6ICwh3VQB1Zhyue6CrbGzpkVJbCgSCeTbD1KER6Cb0cGrqf3twOP9TMW6YX_URCYA3UgauiikzvoCKZV6lsF5nQy1l7jjwpWluPxsHq8DnlV3uVui5Aq_32Gv1yNOUnUl0A8f0DDRrLfB--A7TAUCh8iLEaue_9vaIckyZfNs10ln1exNeLeEcXXFrB1cegpqbdYs4fmEplyxJUuRVlsCL2AQFnFd9MMlKwfIuTjmNtjpvci_blnZN3EJoyx5Kh4x9oTUW6UbWq-WIet-y6kbTgzFE-PtUofSKih4LJUpaHU3Fb2MDenglJLyFzJOh4TnDKwkIQ3SaW183L1L7AXCLr843pBvD2pot2Saa9oM_XBU4lFDwXf3mKcImIbvvB-eF1zjoM50zZ6_FaXL-f0ZU0LyiHB6yHZKC-MnixTWydawd3ciw8rXomcUDvd4wm5U4nzjpk_s9Z2CSwcRVm53VbpSFYaJcBRpPzlatKSM7DMLQQfaXrnj6-FMsh5smgMV4-19JkDgP1FM4tXI9m21I7nOkiD2mi5m2ORmpaLdl6iDV3T7sR0g3QWAnoMGVzmuEGXERQJN968qaHfPk_ve-KfjRZnfmaWeHdVzBaRJh0u6Vmhwcii8chPpf4LrPRDRXrKTqsQ1Iq0meqY9rdR_-d97QaUG1Ekge3yVFiTYN5Yx3EBB-ShbXAbkhI4zeFQ6lJa_CKwhPdn9nMt_D3_OcrTooL-mSXETkePJk22wXkL_M67a2gu_PESV4l-0x6Sniz4L7CYYlIv3TfDlhLWFfhKiGQXSle1hOg7grRDmT-2aRIxTX4Blp-bR8PEWcQZohCMjD1DDrebaUA2npZ3rf7dLW5hdg49Lk8Jhe_Ps-4sPIpBb27q_chDtbkvuyZkwfUw4rQxCzmnEa4-qrfHpeKH27syFPJ54JjnxiaYXhfyCaOEWE1ooHzMO8oAPXJ_NM99Mly3KFg6jPwOdsePgzyR6kWR-NBN4khLB58hKdD2zxhou8uDjmjQ4HGvhSfaQzUg_WCuOtmAo0vSE5H_BxRgG30nacYPK5sacqsHRve_2pvBRqqE5LmckLo_E4cENdsEt1bT2djhtmykg4eC6IYX0qpJBx8hC6iSdR5jjPCNBaHKUC599ezmjCV_yi0YyFQpsBNrlunGoWlUItlO6jorqhqBTRiO0LxIs9kqh2_lY03IakRicAutcvBhG2uBrR9p79yBHBzumhAbqZhwfcy71kXXFGW8O&v=404&clicktype=1&",
            "canClipTitleImg": true,
            "playUrl": "https://jvod.300hu.com/vod/product/54eef7f9-7801-43f8-ac6c-c92c56ca7db7/cd9fe4669ee54d17aaa1b4ab76dbfc33.mp4",
            "duration": 42,
            "exposureJsonSourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6NzcyNTU2MSwicCI6NzEwMDMzLCJzb3VyY2UiOiIxIn0=\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"强劲冲力,微晶抗污釉面,5年质保\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"0\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"5\",\"index\":\"1\",\"liveId\":\"11152018\",\"imp_style\":\"0\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000105384\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"7725561\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "recomReasonStyle": "1",
            "freeShippingFlag": "0",
            "liveSkuStatus": "5",
            "goodsType": 1,
            "promotionType": 1,
            "p": "710033",
            "isArrivalPriceSupportStoreGoods": "0",
            "isArrivalPriceBelt": "1",
            "beltType": "1",
            "purchasePrice": "949",
            "totalBenefit": "50",
            "miniCommentCount": "10万+",
            "isFollow": false,
            "arrivePriceCode": "tab_213",
            "arrivePriceText": "到手价",
            "arrivePriceColor": "#FA2C19",
            "isMiniWareDetailArrivePrice": "1",
            "brokerInfo": "eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6NzcyNTU2MSwicCI6NzEwMDMzLCJzb3VyY2UiOiIxIn0=",
            "shopLogoSquare": "https://img13.360buyimg.com/cms/jfs/t1/200083/3/10634/5357/61551c0eE96a1347f/4624c89c024c58d0.png",
            "miniInteractive": "0",
            "purchaseNum": "1",
            "flow": "101",
            "adword": "",
            "startRemainTime": 0,
            "endRemainTime": 0,
            "sid": "b360ad54-e2dc-4492-9a41-ab9b54976c89",
            "isFeedBackSlide": 0,
            "followCount": "",
            "category1": "9855",
            "category2": "9857",
            "category3": "9910",
            "stockStateId": 33,
            "remainNum": -1,
            "wareHouseNum": "2",
            "clickUrl": "http://ccc-x.jd.local/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC83NzI1NTYxLmh0bWw&log=lX_ecXwcWJG5wH87RGkIpBgF1sW1Rig-k28Si60HsEie-xZYWa91f802L2v-on4is7XX3J0HVt-K4A7EpxYViQVjiYLqHoDw7-DFp1X_eMPXVbh8fEN2B4Z-r84uux110ov5NoVF54uWH0siPDo1GrjYzNN97D7pz2JdGATRLwz5FOoWihZhJCe2ffBj1XP40X9DfTgqcXhtU6-9Q88V2bS3EpdIGG_PHAEaTiCvWufe5DX2noEnFAV-LS5nAGo05XW525PlMDx_d9yfXqnqLRvY5wNOrntN1EkGmqSLMSBGPsDlkd1xgsZwze_HIGiF49yBVgjDp5E0tbHfEAQB8WH1rarxISvJLNqQGGcwnzQbtnYSlICg7jrxTT1Cy2YwcRjdxKnPzgmn4AQoUO3ynhgHYmVEKy12JmOz8HzVQWP2adpNs2SJzqhW8smd-7bCAQMZ3_38Q3aPKqHKvcbhMbiezC-do3K6ICwh3VQB1Zhyue6CrbGzpkVJbCgSCeTbD1KER6Cb0cGrqf3twOP9TMW6YX_URCYA3UgauiikzvoCKZV6lsF5nQy1l7jjwpWluPxsHq8DnlV3uVui5Aq_32Gv1yNOUnUl0A8f0DDRrLfB--A7TAUCh8iLEaue_9vaIckyZfNs10ln1exNeLeEcXXFrB1cegpqbdYs4fmEplyxJUuRVlsCL2AQFnFd9MMlKwfIuTjmNtjpvci_blnZN3EJoyx5Kh4x9oTUW6UbWq-WIet-y6kbTgzFE-PtUofSKih4LJUpaHU3Fb2MDenglJLyFzJOh4TnDKwkIQ3SaW183L1L7AXCLr843pBvD2pot2Saa9oM_XBU4lFDwXf3mKcImIbvvB-eF1zjoM50zZ6_FaXL-f0ZU0LyiHB6yHZKC-MnixTWydawd3ciw8rXomcUDvd4wm5U4nzjpk_s9Z2CSwcRVm53VbpSFYaJcBRpPzlatKSM7DMLQQfaXrnj6-FMsh5smgMV4-19JkDgP1FM4tXI9m21I7nOkiD2mi5m2ORmpaLdl6iDV3T7sR0g3QWAnoMGVzmuEGXERQJN968qaHfPk_ve-KfjRZnfmaWeHdVzBaRJh0u6Vmhwcii8chPpf4LrPRDRXrKTqsQ1Iq0meqY9rdR_-d97QaUG1Ekge3yVFiTYN5Yx3EBB-ShbXAbkhI4zeFQ6lJa_CKwhPdn9nMt_D3_OcrTooL-mSXETkePJk22wXkL_M67a2gu_PESV4l-0x6Sniz4L7CYYlIsKD0-gRIrJlclSBzE8k1QAUDCPm4h1zjqU9AJ2tLTMtMkJLfBQIYmJQPvtpXTg5Yk-wIkO_dPlYWI4pVnep5mOVjwlE_WiGvGJkL1G7ysARC4U9jEL2zsn3kgzUVlT4NfbvcFT7-k9AJQkYkN9ZwX9e3-s93SjkyjTE6h6yU22jUkGG9zXP_p6DE_A0E0SHgWax_fExliufB2PGibqfV0JhsCygH59qcYGruEyPO_q9XtZEPBOWl4A8ZySpZNUFqCKT5Z6atk3L7o5_0QhvDs_IyTg535ZmRERy6-drWADJIZyGIadNh-cs4IRft3iXHkRttEK5LDQN380TVSDcld8N-ynoxBzXwDThOkhp5ATbAEkahs5aJUPQdiS0fi-ijBwFVYlbG-PZvMr5RaWDrtLX8hhFk3XFlV6ZNij8CGVrY97g4Rv6bEAqh1WMU2Q2kfD4mLc_K-UN5Ov67MQrpG7&v=404&clicktype=1&",
            "similarEnter": "1",
            "canAddCart": "1",
            "couponSortType": 0,
            "canNegFeedback": "1",
            "reqsig": "85bcd60a3a615902b25362ea214dcaa798b791a0",
            "abt": "0",
            "isDotScheme": false,
            "presaleWare": 0,
            "renewal": 0,
            "icon2": "tab_232",
            "iconC": "tab_060",
            "tipsMapList": [],
            "venderId": "1000105384",
            "sourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6NzcyNTU2MSwicCI6NzEwMDMzLCJzb3VyY2UiOiIxIn0=\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"强劲冲力,微晶抗污釉面,5年质保\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"0\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"5\",\"index\":\"1\",\"liveId\":\"11152018\",\"imp_style\":\"0\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000105384\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"7725561\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "sourceValueFeedback": "{\"itemid\":\"-100\",\"cvgsku\":\"-100\",\"index\":\"1\",\"page\":\"1\",\"source\":\"1\",\"type\":\"-100\",\"skuid\":\"7725561\",\"flow\":\"101\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"matrt\":\"-100\"}",
            "sourceValueSimilar": "{\"p\":\"710033\",\"index\":\"1\",\"page\":\"1\",\"source\":\"1\",\"skuid\":\"7725561\",\"flow\":\"101\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "source": "1",
            "expid": "-100",
            "feedBackReason": [
                {
                    "name": "不感兴趣",
                    "icon": "feedback_0",
                    "id": 0
                },
                {
                    "name": "商品不够新颖",
                    "icon": "feedback_18",
                    "id": 18
                },
                {
                    "name": "品类不喜欢",
                    "icon": "feedback_1",
                    "id": 1
                },
                {
                    "name": "已经买了",
                    "icon": "feedback_5",
                    "id": 5
                },
                {
                    "name": "商品图引起不适",
                    "icon": "feedback_6",
                    "id": 6
                },
                {
                    "name": "涉及隐私",
                    "icon": "feedback_7",
                    "id": 7
                }
            ],
            "feedBackStrategy": "B",
            "isCoupon": "1",
            "isFlashPurchase": "1",
            "mainVideoId": "850676795",
            "mainVideoImg": "https://jvod.300hu.com/img/2022/209167679/1/img6.jpg",
            "openShopJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"home\"}",
            "miniCanAddCart": "1",
            "sellPointNewFlag": "1",
            "sellPoint": [
                "强劲冲力",
                "微晶抗污釉面",
                "5年质保"
            ],
            "shopId": "1000105384",
            "imgList": [
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/170120/33/24119/105979/61978841E2453378a/549f0e76a763e99a.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/143269/23/21043/73731/61978841E7e6c71d3/807b3f25a5f56fda.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/189489/7/10041/103711/60d3eda3Ef8457ef7/94c386c0c7180ffe.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/64239/7/17379/108460/611f1f99E4c77a9be/2b2d99fa3f918dc8.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/222891/28/3267/120235/61978841E0c26724c/a0d5528dc940dd5b.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/200141/12/17538/99814/61978841E6955fb09/6e1cf4a3a78437da.jpg!q70.dpg"
            ],
            "wareBusiness": {
                "code": 0,
                "floors": [
                    {
                        "bId": "eCustom_flo_614",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "topOn": false,
                            "isOpenCar": true,
                            "supportSale": true,
                            "abTest800": true,
                            "yuyueInfo": {
                                "isYuYue": false,
                                "isbuyTime": false,
                                "mad": false,
                                "plusType": 0,
                                "showDraw": false,
                                "yuYue": false,
                                "yuyueNum": 0
                            },
                            "isOpenNode": true,
                            "property": {
                                "isJzfp": false,
                                "newReceiveCoupon": true,
                                "suitABTest": 0,
                                "buyMaxNum": 200,
                                "isJnbtNewText": false,
                                "plus20Flag": false,
                                "isOverseaPurchase": "0",
                                "easyBuy": true,
                                "isFxyl": false,
                                "isRegisterUser": false,
                                "isFlimPrint": "",
                                "isShowShopNameB": false,
                                "dpgRevisionFlag": true,
                                "showEmptyPrice": false,
                                "threeSuperFlag": false,
                                "noStockOrder": false,
                                "virtualCardUrl": "https://gamerecg.m.jd.com?skuId=7725561&chargeType=9910&skuName=恒洁(HEGII)马桶×京东 国民家居 节水防臭 虹吸式 速冲除污连体坐便器HC0171PT0E（坑距305mm）&skuPrice=999.00",
                                "isfans": true,
                                "batchReceiveCoupon": true,
                                "addAndSubToast": {
                                    "lowestToastText": "最少购买1件哦！"
                                },
                                "isEasyBuyPrice": true,
                                "isRx": false,
                                "sdkDegrade": false,
                                "stockNotice": false,
                                "opForIos": true,
                                "isCollect": false,
                                "recTabEnable": true,
                                "isJx": false,
                                "isOTC": "0",
                                "cartFlag": true,
                                "wareImageTest": "",
                                "brandId": "347502",
                                "isOP": false,
                                "isPop": false,
                                "androidImageSwitch": true,
                                "shareUrl": "https://item.m.jd.com/product/7725561.html",
                                "chatUrl": "https://m.360buyimg.com/n3/jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg",
                                "evaluateTabEnable": false,
                                "category": "9855;9857;9910",
                                "isRegularPrice": true,
                                "isEncrypt": true
                            },
                            "otherUseBannerInfo": {
                                "flashDegrade": false,
                                "bannerText": "使用优惠预估价",
                                "koDegrade": false,
                                "bannerPrice": "¥949.00",
                                "businessEnum": "1"
                            },
                            "leVieuxFusil": false,
                            "newStyle": false,
                            "priceLabel": "¥",
                            "abTestInfo": {
                                "addCarRecommendAB": false,
                                "askMedicineInfoAb": false,
                                "attentionAB": "A",
                                "collectABInfo": {
                                    "bottom3ABTest": true,
                                    "bottom4ABTest": true,
                                    "titleABTest": true
                                },
                                "easyDelAB": true,
                                "evaluateAB": false,
                                "feedBackAB": true,
                                "hasChangeButton": true,
                                "hospitalAB": "A",
                                "hwShare": false,
                                "newUser": false,
                                "newuserFreeAb": false,
                                "noBotmShop": false,
                                "packABTest": 1,
                                "qaUpAb": false,
                                "rankYhTag": true,
                                "recommendPopup": false,
                                "recommendYhTag": true,
                                "shareM": "a",
                                "shareShield": false,
                                "shopABTest": "a",
                                "shopCardTypeAb": true,
                                "shopExtendsAb": true,
                                "shopIntensifyAB": true,
                                "shopPromotionAB": "B",
                                "show3cService": true,
                                "showBuyLayer": true,
                                "skuSource": 2,
                                "specialSelectAB": true,
                                "toABTest": true
                            },
                            "shopInfo": {
                                "customerService": {
                                    "hasChat": false,
                                    "hasJimi": false,
                                    "allGoodJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"allProduct\"}",
                                    "mLink": "https://m.jd.com/product/7725561.html",
                                    "online": false,
                                    "inShopLookJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"home\"}"
                                }
                            },
                            "flashInfo": {
                                "banner": "https://m.360buyimg.com/mobilecms/jfs/t3079/270/5085309438/2440/77dba8ec/5861cc06N113d618f.png",
                                "cd": 98512,
                                "cdBanner": "https://m.360buyimg.com/mobilecms/jfs/t3208/230/5077057080/2463/853a9aa/5861ccb7N731e030d.png",
                                "desc": "距闪购结束还剩:",
                                "endTime": 1662566400000,
                                "icon": "detail_028",
                                "promoPrice": "999",
                                "startTime": 1662465600000,
                                "state": 2
                            },
                            "isDesCbc": true,
                            "isNewPP": true,
                            "isOpen": true,
                            "toHandssSrengthen": {
                                "buttonShow": true,
                                "selectShow": true,
                                "predictText": "预估到手价",
                                "toHandsPrice": "¥949",
                                "toHandsText2Yugu": true,
                                "toHandsText": "到手价"
                            },
                            "jumpToProduct": false,
                            "jdwlType": 0,
                            "eventParam": {
                                "sep": "{\"area\":\"1_2800_55837_0\",\"sku\":[[\"7725561\",\"999.00\",\"现货,明日19:00前下单，预计(09月08日)送达\",\"33\",\"0\"]]}"
                            },
                            "priceInfo": {
                                "rangePriceFlag": false,
                                "jprice": "999.00"
                            },
                            "showAttentionButton": true,
                            "tenthRevision": false,
                            "attentionInfo": {
                                "txt2": "已收藏",
                                "txt1": "收藏"
                            },
                            "weightInfo": {
                                "content": "不计重量",
                                "title": "重量"
                            },
                            "buttonInfo": {
                                "main": {
                                    "bgColor": "#FFA600,#FFB000,#FFBC00;#E59500,#E59B00,#E6A800;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "立即购买",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 1
                                },
                                "second": {
                                    "bgColor": "#F10000,#FF2000,#FF4F18;#D80000,#E51C00,#E54615;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "加入购物车",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 0
                                }
                            },
                            "bottomButtonInfo": {
                                "leftButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 1,
                                    "buttonHightColor": "#D80000,#E51C00,#E54615",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#F10000,#FF2000,#FF4F18",
                                    "buttonPriority": 240,
                                    "buttonText": "加入购物车",
                                    "buttonTextColor": "#ffffffff"
                                },
                                "rightButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 2,
                                    "buttonHightColor": "#E59500,#E59B00,#E6A800",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#FFA600,#FFB000,#FFBC00",
                                    "buttonPriority": 240,
                                    "buttonSubText": "到手价¥949",
                                    "buttonText": "立即购买",
                                    "buttonTextColor": "#ffffffff"
                                }
                            },
                            "pointInfo": {
                                "isShowAr": false,
                                "isOPType": 0
                            },
                            "wareImage": [
                                {
                                    "small": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg!q70.dpg.webp",
                                    "big": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg!q70.dpg.webp",
                                    "share": "https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/101233/25/32131/72438/6311ceaeEe03ba49c/4d460cbde85bc570.jpg!q70.jpg"
                                }
                            ],
                            "oldMoreStyle": false,
                            "wareInfo": {
                                "name": "恒洁(HEGII)马桶×京东 国民家居 节水防臭 虹吸式 速冲除污连体坐便器HC0171PT0E（坑距305mm）",
                                "venderId": "1000105384",
                                "skuId": "7725561"
                            },
                            "promotionInfo": {
                                "isBargain": false,
                                "prompt": ""
                            },
                            "tabUrl": "https://in.m.jd.com/product/graphext_0_0_0_0_0/7725561.html",
                            "isOpenH5": true,
                            "isScf": false,
                            "locCouponFlag": false,
                            "generalTrackDic": {
                                "shopId": "1000105384",
                                "skuId": "7725561"
                            },
                            "ddFatigueMechanism": {
                                "leftBottomBubbleCount": "5",
                                "leftBottomBubbleDuration": "5",
                                "ddBottomToastDuration": "10",
                                "ddBottomToastCount": "5",
                                "ddRightCount": "5"
                            },
                            "report": {
                                "darkIcon": "https://m.360buyimg.com/cc/jfs/t1/207978/19/1794/611/614c4aabE2b82675b/68307d0266c3aab7.png",
                                "icon": "https://m.360buyimg.com/cc/jfs/t1/199632/26/9352/400/614c4a83E7615a396/11f6a5f4c1eb8497.png",
                                "isOpen": true,
                                "jumpUrl": "https://h5.m.jd.com/babelDiy/Zeus/2WH8HhtMB5TQ2KcjAbrrW3atvo68/index.html#/home?pin=jd_61fdcba78e4b7&sku=7725561",
                                "title": "我要反馈"
                            },
                            "appointAndPresaleOpen": true,
                            "plusShieldLandedPriceFlag": false,
                            "shareImgInfo": {
                                "priceDes": "价格具有时效性",
                                "promotionStr": "",
                                "markingOff": true,
                                "shareLanguage": "推荐一个好物给你，请查收",
                                "secondPrice": "1399.00",
                                "jprice": "999.00",
                                "priceUrl": "https://m.360buyimg.com/cc/jfs/t1/32239/11/7019/5065/5c933b3dE4a485f97/f3a371d0407ef626.png"
                            }
                        },
                        "mId": "miniMasterdata",
                        "sortId": 1
                    },
                    {
                        "bId": "eCustom_flo_615",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "preferentialGuide": {
                                "secondLevelGuideInfo": {
                                    "sLevelGuideText1": "使用以下“*”标优惠，预估价¥949.00 ",
                                    "sLevelIconCode": "https://m.360buyimg.com/cc/jfs/t1/75639/12/11713/385/5d91bd73E9865f7db/4122771acdea2dc7.png",
                                    "sLevelGuideText2": "以上优惠仅为初步预估，不代表最终价格，规则可在帮助中心查看",
                                    "sLevelJumpUrl": "https://ihelp.jd.com/l/help/scene/getSceneDetail?id=322666",
                                    "saveMoney": "¥949.00",
                                    "sLevelCode": "*"
                                },
                                "couponInfo": [
                                    {
                                        "addDays": 0,
                                        "anotherType": 0,
                                        "applicability": true,
                                        "area": 1,
                                        "batchId": 924197693,
                                        "beginHour": "20:00",
                                        "beginTime": "2022.09.06",
                                        "businessLabel": "0",
                                        "couponIcon": [],
                                        "couponId": "924197693",
                                        "couponKind": 1,
                                        "couponStyle": 0,
                                        "couponType": 1,
                                        "discount": 200,
                                        "discountText": "以下商品可使用满3000减200的优惠券",
                                        "encryptedKey": "DJ/nkkCwHhG4kRKEZvJmnj9ygzbEBq4z3W8ZfQcbLgo4CfEx2VF27w==",
                                        "endHour": "23:59",
                                        "endTime": "2022.09.10",
                                        "gwcCoupon": {
                                            "appid": "warecoresoa",
                                            "batchId": "924197693",
                                            "biinfo": "-100",
                                            "channel": "商品详情",
                                            "channelDetail": "-100",
                                            "couponPos": "可领券#0",
                                            "couponSource": "cmcGuide",
                                            "couponStatus": "0",
                                            "getType": 1,
                                            "platformid": "app-itemdetail",
                                            "skus": "7725561",
                                            "subChannel": "优惠弹层"
                                        },
                                        "isMultipleDiscount": false,
                                        "isOverlap": false,
                                        "isPlusCoupon": false,
                                        "labelTxt": "限品类东券",
                                        "limitType": 5,
                                        "milliSecond": 0,
                                        "name": "仅可购买恒洁部分商品",
                                        "personalCoupon": false,
                                        "platform": 0,
                                        "quota": 3000,
                                        "roleId": 0,
                                        "roleIdCBC": "RWWe5zptEoXC1ThnMAfYaA==",
                                        "timeDesc": "有效期2022-09-06 20:00至2022-09-10 23:59",
                                        "toUrl": "www.jd.com,m.jd.com",
                                        "userRiskLevel": 0
                                    }
                                ],
                                "iconCode": "detail_var_045",
                                "couponIconText": "领券",
                                "firstLevelGuideInfo": {
                                    "fLevelGuideText": "使用优惠预估价¥949.00 ",
                                    "saveMoney": "¥949.00",
                                    "fLevelCode": "detail_047"
                                },
                                "businessEnum": "1",
                                "promotion": {
                                    "activity": [],
                                    "activityTypes": [
                                        "15"
                                    ],
                                    "attach": [],
                                    "bestProList": [
                                        {
                                            "value": "每满799元，可减50元现金",
                                            "text": "满减",
                                            "skuId": "",
                                            "proSortNum": 40,
                                            "link": "",
                                            "proId": "200374297926",
                                            "promoId": "200374297926"
                                        }
                                    ],
                                    "canReturnHaggleInfo": false,
                                    "gift": [],
                                    "isBargain": false,
                                    "isTwoLine": true,
                                    "limitBuyInfo": {
                                        "noSaleFlag": "0",
                                        "limitNum": "0",
                                        "strategyFlag": "0",
                                        "isPlusLimit": "0",
                                        "limitUserFlag": "1",
                                        "limitAreaFlag": "1",
                                        "limitPeopleFlag": "1"
                                    },
                                    "normalMark": "tab_var_071",
                                    "plusDiscountMap": {},
                                    "plusMark": "tab_var_124",
                                    "prompt": "",
                                    "screenLiPurMap": {},
                                    "tip": "",
                                    "tips": []
                                },
                                "hasFinanceCoupon": false
                            }
                        },
                        "mId": "miniAggrePromo",
                        "sortId": 2
                    }
                ],
                "others": {
                    "templateType": "mini"
                },
                "wait": 0
            },
            "jdShop": false
        },
        {
            "itemType": 0,
            "rt": 0,
            "wareId": "100033963110",
            "wname": "恒洁(HEGII)马桶×京东 国民家居 全包易清洁 节水防臭 虹吸式 大冲力连体坐便器HC0506PT（坑距305mm）",
            "markType": 0,
            "isMonetized": false,
            "imageurl": "https://m.360buyimg.com/mobilecms/s714x714_jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg!q70.dpg",
            "imageurlType": 0,
            "good": "",
            "commentCount": "",
            "jdPrice": "1399.00",
            "isSamWare": false,
            "isPlusWare": false,
            "isPinGouWare": false,
            "isFansWare": false,
            "book": "false",
            "promotion": "false",
            "commentAbStrategy": "A",
            "mp": 0,
            "feminine": false,
            "extension_id": "{\"ad\":\"5651\",\"ch\":\"2\",\"sku\":\"100033963110\",\"ts\":\"1662467887\",\"uniqid\":\"{\\\"material_id\\\":\\\"7190602767\\\",\\\"pos_id\\\":\\\"5651\\\",\\\"sid\\\":\\\"b360ad54-e2dc-4492-9a41-ab9b54976c89\\\"}\"}",
            "samePicPid": "0-19539-0-73-0-19590",
            "opPrice": "1799.00",
            "interactive": "0",
            "client_exposal_url": "https://sh.jd.com/d?fl=4UwrzuWaEDvFq4RKp6KkGk1GF_ZZyII6vT5Z9Y-Bdc-h-h4AbDxEvyvMKk1jxrfNSyjz1bDTvl7573DRFECt4YJM2PkSPNfv9UIXAm1Rx0cLAqfiNIlTmXf5-h_m3z3SnZ6ksTS_20YyzFix0hBq3uQdmQUoefLRW1JhxD1x6E6Mw_83AIPiBzvpi-G6N27idwKDsRc-w8HT5sgjQyg04A",
            "client_click_url": "https://ccc-x.jd.com/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC8xMDAwMzM5NjMxMTAuaHRtbA&log=lX_ecXwcWJG5wH87RGkIpBgF1sW1Rig-k28Si60HsEhJ5fZ468obpbKDHEQuaKvGm2aV6lxthyiF1FgD3XZ7xbvfuwCWpCHJ4OCXZBNDrbV1GetNuoa7l9Z2DnVQwovVeKN7_gLtnhB-8RUz51JGKlPAWBsFHRUu5b8TXBFgMEbK0nIwc9cT1gNj45znhz0Pe8XNgeY0jbthGgkWZk5VgKWvagM4TzwSCT6CgxJXKSDGCYe4ZKhPVq5-ZikcQxnH9L4XRv1oBG9Cm6mKu8QAg9VoIfL3ujMjiOjuPMoMmcXJdQHjw5KDG1i_EVnD2KMPEcgroSiVd0Lkd8IWy6yh68s94uiGcEyH3B67MMM2ZlN6I_ciEvzdYh-562QDstqw2CrJhWMKO_xyntzOdPnR3epzFoc0IymFbZ9CDR70shkbRPcr45XRw_qrXpt9oztBqbkIOW26zeRm7NT0e3xwOl2KBfwz7mz9TJzpXi2aVQS5q25HDiuMu_bgaQVH_YpZXM2-V0aLYbAGNhFo9ifYhZy56wQugQuW3FL05wFzz0VpC_jocaU2rBgo58k1liRwUXJoudR4UNkL_OnPIDvdaz51rAZUkwn1tccooF0N_n1o9GYKuvlutSJX_N8vxmPVQRQDy0DOniUxR824p8hmGXJx7NCUFmIZfOxBVLCky93AzUgVPGdjh9_Huqqr3J3d2geY1VXTeZOs23yv3_eBqU-yFinr5dXxSnXmA-v0KQRzCfIZrwkXPfGXD_uInJ1nVhRWvRq-XfYVkQ7uUHA20Jm7AmHcOnNRdQjrud7UhSirPTmjt34eCnxghw08hWUIOVPNpF2PNbIz4bH0wOoYoSJhTz-6ZYirQk8IVJZ88AclK5GuK-YM5rNpmtyjvXnhOIWx-qDebJ3f3pqSRUI3718zt9PmywvzjM6VEIIrEAW_3hZs88XJSlY5K61hNRwfxo9gS_lAlvrMqfg8Am9EAfT9HJFmYW1J9datIuPmiCKhVBHAzMwjES9ECVZUyTSp-3vDdXPug1Zl2_iqZL_eWapi0Ej9My3bzbN2eRLNIXRPZFqxYjtUvrB3H9tNe5ua3MI3zk6mbEFf7qkh47Xmrl6oC8RFMiG7UohEPL_mf-NGDelr_XhqdPEqYpZ0IkkaGFtjbFPqdUWMBjx2h27bREcYB4spv3WHGzwQq36zFv8__2NyR5skgyrmb2ubBpk8KhwTPWnNGXiXAEYHKWzCGbNN_JJ9qqq79E5257DymTXOAI-Ed4tMiCny2rHpA-pdk_PNmKCoacUYgOgyx5yqadtZSR8oti-nefzmPmwbP4ftvwFvmNQN1_aSGoI_a45Ddrue4ngDLdIsQ84muBkPYP2_jrbI199rmWV2CU-qkyG9Rv2Cavy4Ou9xPMiVzv6sTuk2-eSwvKNVa0-2E3ArPokYYWkKYiYXllf6jK7oJ605Pui7EdqZAkXSB0MnifL8MbjgUf2tIriUaj6SWBGt-lpeu1-33qAYPgNd51up2Cs7yYV2h43zLPX6FmUe2w6DWLg29JLZ1iC9OqQxh5TimATBJ7_fUW7eh9acVPkfXuvUlQqX9_-1YZU5NaomqpF3FhePY9rtIA31ME3GPK8bPrUaGW8glzGZtw-GbkcH07mjyFfdk2Sin82OsXbFkAgLWl3Jnb2W913_OS4mCltYpJjxf9kggaKSNGjW3WlmZCVll2lpYz3DvnYbu6mg1zrM&v=404&clicktype=1&",
            "canClipTitleImg": true,
            "playUrl": "https://jvod.300hu.com/vod/product/d6f00e35-abee-45b9-897c-8c5b80890aa7/91e5b0c73b1b4715997310dacefb954d.mp4",
            "duration": 48,
            "exposureJsonSourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDMzOTYzMTEwLCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"双C曲面设计,纤薄轻盈盖板\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"0\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"0\",\"index\":\"2\",\"liveId\":\"-100\",\"imp_style\":\"0\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000105384\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"100033963110\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "recomReasonStyle": "1",
            "freeShippingFlag": "0",
            "liveSkuStatus": "0",
            "goodsType": 1,
            "promotionType": 1,
            "p": "710033",
            "isArrivalPriceSupportStoreGoods": "0",
            "isArrivalPriceBelt": "1",
            "beltType": "1",
            "purchasePrice": "1199",
            "totalBenefit": "200",
            "miniCommentCount": "1万+",
            "isFollow": false,
            "arrivePriceCode": "tab_213",
            "arrivePriceText": "到手价",
            "arrivePriceColor": "#FA2C19",
            "isMiniWareDetailArrivePrice": "1",
            "brokerInfo": "eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDMzOTYzMTEwLCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==",
            "shopLogoSquare": "https://img13.360buyimg.com/cms/jfs/t1/200083/3/10634/5357/61551c0eE96a1347f/4624c89c024c58d0.png",
            "miniInteractive": "0",
            "purchaseNum": "1",
            "flow": "101",
            "adword": "",
            "startRemainTime": 0,
            "endRemainTime": 0,
            "sid": "b360ad54-e2dc-4492-9a41-ab9b54976c89",
            "isFeedBackSlide": 0,
            "followCount": "",
            "category1": "9855",
            "category2": "9857",
            "category3": "9910",
            "stockStateId": 33,
            "remainNum": -1,
            "wareHouseNum": "2",
            "clickUrl": "http://ccc-x.jd.local/dsp/nc?ext=aHR0cDovL2l0ZW0ubS5qZC5jb20vcHJvZHVjdC8xMDAwMzM5NjMxMTAuaHRtbA&log=lX_ecXwcWJG5wH87RGkIpBgF1sW1Rig-k28Si60HsEhJ5fZ468obpbKDHEQuaKvGm2aV6lxthyiF1FgD3XZ7xbvfuwCWpCHJ4OCXZBNDrbV1GetNuoa7l9Z2DnVQwovVeKN7_gLtnhB-8RUz51JGKlPAWBsFHRUu5b8TXBFgMEbK0nIwc9cT1gNj45znhz0Pe8XNgeY0jbthGgkWZk5VgKWvagM4TzwSCT6CgxJXKSDGCYe4ZKhPVq5-ZikcQxnH9L4XRv1oBG9Cm6mKu8QAg9VoIfL3ujMjiOjuPMoMmcXJdQHjw5KDG1i_EVnD2KMPEcgroSiVd0Lkd8IWy6yh68s94uiGcEyH3B67MMM2ZlN6I_ciEvzdYh-562QDstqw2CrJhWMKO_xyntzOdPnR3epzFoc0IymFbZ9CDR70shkbRPcr45XRw_qrXpt9oztBqbkIOW26zeRm7NT0e3xwOl2KBfwz7mz9TJzpXi2aVQS5q25HDiuMu_bgaQVH_YpZXM2-V0aLYbAGNhFo9ifYhZy56wQugQuW3FL05wFzz0VpC_jocaU2rBgo58k1liRwUXJoudR4UNkL_OnPIDvdaz51rAZUkwn1tccooF0N_n1o9GYKuvlutSJX_N8vxmPVQRQDy0DOniUxR824p8hmGXJx7NCUFmIZfOxBVLCky93AzUgVPGdjh9_Huqqr3J3d2geY1VXTeZOs23yv3_eBqU-yFinr5dXxSnXmA-v0KQRzCfIZrwkXPfGXD_uInJ1nVhRWvRq-XfYVkQ7uUHA20Jm7AmHcOnNRdQjrud7UhSirPTmjt34eCnxghw08hWUIOVPNpF2PNbIz4bH0wOoYoSJhTz-6ZYirQk8IVJZ88AclK5GuK-YM5rNpmtyjvXnhOIWx-qDebJ3f3pqSRUI3718zt9PmywvzjM6VEIIrEAW_3hZs88XJSlY5K61hNRwfxo9gS_lAlvrMqfg8Am9EAfT9HJFmYW1J9datIuPmiCKhVBHAzMwjES9ECVZUyTSp-3vDdXPug1Zl2_iqZL_eWapi0Ej9My3bzbN2eRLNIXRPZFqxYjtUvrB3H9tNe5ua3MI3zk6mbEFf7qkh47Xmrl6oC8RFMiG7UohEPL_mf-NGDelr_XhqdPEqYpZ0IkkaGFtjbFPqdUWMBjx2h27bREcYB4spv3WHGzwQq36zFv8__2NyR5skgyrmb2ubBpk8KhwTPWnNGXiXAEYHKWzCGbNN_JJ9qqq79E5257DymTUqxKxvhjgBQEIYMI5dQ02IQ_yyDF9GRiuZqeHR2t6gmLm8IQQa-Yw99nx0Y53O9AEDirPI9IRsqCPejLneeg-2nXA2R9ZzmnEY9jw0CMl2wo3se5woYQst7U8alTEAR8keXsFdZUGEyxJ40vmaodpLy5v6h8vgFrUgbfjypjF4pi1-CaGvRSOfTlib3Dv4QTRGXyrfRz8kFbgq6WFStwMTbGscH4tmbmHdlQN-DzTCoNaRkvFgF8-9QIQqttmyP2VKV4QcvU6gbLOOe4GUBGpDu2TOl-QAccGbRaAH1hkL9736ib6wZn_LkI6EWB5SgEiJ_DRbfICd8rMbCHzExNaj8XTNT892OzvFd_hnxGLTsvOlyAGU_ybBKXE4AE2ZnGYegUPLF7_pu_nEnOaoWtt2Bins5UjyjJmcpaUZ4snb1Wk2cSqxL_6lZX2X8y-hOZ8ZaXjv0UryI1OrbKTKQzpg&v=404&clicktype=1&",
            "similarEnter": "1",
            "canAddCart": "1",
            "couponSortType": 0,
            "canNegFeedback": "1",
            "reqsig": "85bcd60a3a615902b25362ea214dcaa798b791a0",
            "abt": "0",
            "isDotScheme": false,
            "presaleWare": 0,
            "renewal": 0,
            "icon2": "tab_232",
            "iconC": "tab_060",
            "tipsMapList": [],
            "venderId": "1000105384",
            "sourceValue": "{\"broker_info\":\"eyJyZXF1ZXN0X2lkIjoiYTBiY2M1YWItNTFjMy00MTY3LTgxZDItOGQxODYxZGYxYTM1IiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9FQ09fUixtYnxHKlpeUl5BX05OX0NMX0wxOTcyOSIsInNrdV9pZCI6MTAwMDMzOTYzMTEwLCJwIjo3MTAwMzMsInNvdXJjZSI6IjEifQ==\",\"psource\":\"47\",\"shopBelt\":\"-100\",\"shopsale\":\"-100\",\"rec_broker\":\"eyJyZXF1ZXN0X2lkIjoiNzdmMjQxYmMtMTU0ZS00ZTBmLTljZDItZGE3ZTIzODkzYTcxIiwicGFsYW50aXJfZXhwaWRzIjoiWl5SXkF8TUlYVEFHX1peUl5BUixaXlJeQV9OTl9DTF9MOTcyNCxaXlJeQV9OTl9YRjAwMV9SLFpeUl5BX05OX0VDT19SLG1ifEcqWl5SXkFfTk5fWEYwMDFfTDE5MzY3LEcqWl5SXkFfTk5fQ0xfTDE5NzI5LEcqWl5SXkFfTk5fWEYwMDFfTDIwODgyLEcqWl5SXkFfTk5fWEYwMDFfTDIwODk5Iiwic2t1X2lkIjoxMDAwMjA0MDc2MzgsInAiOjY2MDAwMCwic291cmNlIjoiMSJ9\",\"channel\":\"4\",\"source\":\"1\",\"entersource\":\"1\",\"title\":\"1\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"matching\":\"-100\",\"specs\":\"-100\",\"feature\":\"双C曲面设计,纤薄轻盈盖板\",\"pricetype\":\"到手价\",\"materiaid\":\"-100\",\"saleinfo\":\"-100\",\"sameShopNum\":\"0\",\"imp_sub_style\":\"-100\",\"flow\":\"101\",\"liveSkuStatus\":\"0\",\"index\":\"2\",\"liveId\":\"-100\",\"imp_style\":\"0\",\"sub_tagList\":\"-100\",\"p\":\"710033\",\"tagList\":\"-100\",\"is_cutvideo\":\"0\",\"skutag_type\":\"0\",\"playshow\":\"1\",\"shopid\":\"1000105384\",\"page\":\"1\",\"mbox_type\":\"-100\",\"supInfo\":\"2\",\"skutype\":\"京东新百货\",\"jumptype\":\"0\",\"skuid\":\"100033963110\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "sourceValueFeedback": "{\"itemid\":\"-100\",\"cvgsku\":\"-100\",\"index\":\"2\",\"page\":\"1\",\"source\":\"1\",\"type\":\"-100\",\"skuid\":\"100033963110\",\"flow\":\"101\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\",\"matrt\":\"-100\"}",
            "sourceValueSimilar": "{\"p\":\"710033\",\"index\":\"2\",\"page\":\"1\",\"source\":\"1\",\"skuid\":\"100033963110\",\"flow\":\"101\",\"sid\":\"b360ad54-e2dc-4492-9a41-ab9b54976c89\",\"expid\":\"-100\",\"reqsig\":\"85bcd60a3a615902b25362ea214dcaa798b791a0\"}",
            "source": "1",
            "expid": "-100",
            "feedBackReason": [
                {
                    "name": "不感兴趣",
                    "icon": "feedback_0",
                    "id": 0
                },
                {
                    "name": "商品不够新颖",
                    "icon": "feedback_18",
                    "id": 18
                },
                {
                    "name": "品类不喜欢",
                    "icon": "feedback_1",
                    "id": 1
                },
                {
                    "name": "已经买了",
                    "icon": "feedback_5",
                    "id": 5
                },
                {
                    "name": "商品图引起不适",
                    "icon": "feedback_6",
                    "id": 6
                },
                {
                    "name": "涉及隐私",
                    "icon": "feedback_7",
                    "id": 7
                }
            ],
            "feedBackStrategy": "B",
            "isCoupon": "1",
            "isFlashPurchase": "1",
            "mainVideoId": "850675606",
            "mainVideoImg": "https://jvod.300hu.com/img/2022/209163493/1/img7.jpg",
            "openShopJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"home\"}",
            "miniCanAddCart": "1",
            "sellPointNewFlag": "1",
            "sellPoint": [
                "双C曲面设计",
                "纤薄轻盈盖板"
            ],
            "shopId": "1000105384",
            "imgList": [
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/195645/2/26167/174886/62c7edf4Eef1fea74/c5ec855f1fc2f3db.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/115396/39/27877/88962/62c7ee03E1cae43ab/09e1d2e36f2812c7.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/126126/26/28922/72754/62c7ee0fEa7d07f88/56bef2903f80fa00.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/213529/16/20540/70548/62c7ee16E1dadbcf6/252cb2f39fc5ffa9.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/200016/34/24624/133922/62c7ee09E15a246e0/f0a139c9b802393c.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/200153/38/25153/94345/62c7edfeE84c5db20/31cbacb12bcb14bd.jpg!q70.dpg",
                "https://m.360buyimg.com/mobilecms/s1352x1352_jfs/t1/160479/12/28827/49140/62183eb7E2507de04/85f084afcfb5345b.jpg!q70.dpg"
            ],
            "wareBusiness": {
                "code": 0,
                "floors": [
                    {
                        "bId": "eCustom_flo_614",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "topOn": false,
                            "isOpenCar": true,
                            "supportSale": true,
                            "abTest800": true,
                            "yuyueInfo": {
                                "isYuYue": false,
                                "isbuyTime": false,
                                "mad": false,
                                "plusType": 0,
                                "showDraw": false,
                                "yuYue": false,
                                "yuyueNum": 0
                            },
                            "isOpenNode": true,
                            "property": {
                                "isJzfp": false,
                                "newReceiveCoupon": true,
                                "suitABTest": 0,
                                "buyMaxNum": 200,
                                "isJnbtNewText": false,
                                "plus20Flag": false,
                                "isOverseaPurchase": "0",
                                "easyBuy": true,
                                "isFxyl": false,
                                "isRegisterUser": false,
                                "isFlimPrint": "",
                                "isShowShopNameB": false,
                                "dpgRevisionFlag": true,
                                "showEmptyPrice": false,
                                "threeSuperFlag": false,
                                "noStockOrder": false,
                                "virtualCardUrl": "https://gamerecg.m.jd.com?skuId=100033963110&chargeType=9910&skuName=恒洁(HEGII)马桶×京东 国民家居 全包易清洁 节水防臭 虹吸式 大冲力连体坐便器HC0506PT（坑距305mm）&skuPrice=1399.00",
                                "isfans": true,
                                "batchReceiveCoupon": true,
                                "addAndSubToast": {
                                    "lowestToastText": "最少购买1件哦！"
                                },
                                "isEasyBuyPrice": true,
                                "isRx": false,
                                "sdkDegrade": false,
                                "stockNotice": false,
                                "opForIos": true,
                                "isCollect": false,
                                "recTabEnable": true,
                                "isJx": false,
                                "isOTC": "0",
                                "cartFlag": true,
                                "wareImageTest": "",
                                "brandId": "347502",
                                "isOP": false,
                                "isPop": false,
                                "androidImageSwitch": true,
                                "shareUrl": "https://item.m.jd.com/product/100033963110.html",
                                "chatUrl": "https://m.360buyimg.com/n3/jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg",
                                "evaluateTabEnable": false,
                                "category": "9855;9857;9910",
                                "isRegularPrice": true,
                                "isEncrypt": true
                            },
                            "otherUseBannerInfo": {
                                "flashDegrade": false,
                                "bannerText": "使用优惠预估价",
                                "koDegrade": false,
                                "bannerPrice": "¥1199.00",
                                "businessEnum": "1"
                            },
                            "leVieuxFusil": false,
                            "newStyle": false,
                            "priceLabel": "¥",
                            "abTestInfo": {
                                "addCarRecommendAB": false,
                                "askMedicineInfoAb": false,
                                "attentionAB": "A",
                                "collectABInfo": {
                                    "bottom3ABTest": true,
                                    "bottom4ABTest": true,
                                    "titleABTest": true
                                },
                                "easyDelAB": true,
                                "evaluateAB": false,
                                "feedBackAB": true,
                                "hasChangeButton": true,
                                "hospitalAB": "A",
                                "hwShare": false,
                                "newUser": false,
                                "newuserFreeAb": false,
                                "noBotmShop": false,
                                "packABTest": 1,
                                "qaUpAb": false,
                                "rankYhTag": true,
                                "recommendPopup": false,
                                "recommendYhTag": true,
                                "shareM": "a",
                                "shareShield": false,
                                "shopABTest": "a",
                                "shopCardTypeAb": true,
                                "shopExtendsAb": true,
                                "shopIntensifyAB": true,
                                "shopPromotionAB": "B",
                                "show3cService": true,
                                "showBuyLayer": true,
                                "skuSource": 2,
                                "specialSelectAB": true,
                                "toABTest": true
                            },
                            "shopInfo": {
                                "customerService": {
                                    "hasChat": false,
                                    "hasJimi": false,
                                    "allGoodJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"allProduct\"}",
                                    "mLink": "https://m.jd.com/product/100033963110.html",
                                    "online": false,
                                    "inShopLookJumpUrl": "openApp.jdMobile://virtual?params={\"category\":\"jump\",\"des\":\"jshopMain\",\"shopId\":\"1000105384\",\"venderId\":\"1000105384\",\"jumpTab\":\"home\"}"
                                }
                            },
                            "flashInfo": {
                                "banner": "https://m.360buyimg.com/mobilecms/jfs/t3079/270/5085309438/2440/77dba8ec/5861cc06N113d618f.png",
                                "cd": 98512,
                                "cdBanner": "https://m.360buyimg.com/mobilecms/jfs/t3208/230/5077057080/2463/853a9aa/5861ccb7N731e030d.png",
                                "desc": "距闪购结束还剩:",
                                "endTime": 1662566400000,
                                "icon": "detail_028",
                                "promoPrice": "1399",
                                "startTime": 1662465600000,
                                "state": 2
                            },
                            "isDesCbc": true,
                            "isNewPP": true,
                            "isOpen": true,
                            "toHandssSrengthen": {
                                "buttonShow": true,
                                "selectShow": true,
                                "predictText": "预估到手价",
                                "toHandsPrice": "¥1199",
                                "toHandsText2Yugu": true,
                                "toHandsText": "到手价"
                            },
                            "jumpToProduct": false,
                            "jdwlType": 0,
                            "eventParam": {
                                "sep": "{\"area\":\"1_2800_55837_0\",\"sku\":[[\"100033963110\",\"1399.00\",\"现货,明日19:00前下单，预计(09月08日)送达\",\"33\",\"0\"]]}"
                            },
                            "priceInfo": {
                                "rangePriceFlag": false,
                                "jprice": "1399.00"
                            },
                            "showAttentionButton": true,
                            "tenthRevision": false,
                            "attentionInfo": {
                                "txt2": "已收藏",
                                "txt1": "收藏"
                            },
                            "weightInfo": {
                                "content": "不计重量",
                                "title": "重量"
                            },
                            "buttonInfo": {
                                "main": {
                                    "bgColor": "#FFA600,#FFB000,#FFBC00;#E59500,#E59B00,#E6A800;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "立即购买",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 1
                                },
                                "second": {
                                    "bgColor": "#F10000,#FF2000,#FF4F18;#D80000,#E51C00,#E54615;#bfbfbf,#bfbfbf,#bfbfbf",
                                    "name": "加入购物车",
                                    "source": 0,
                                    "textColor": "#ffffffff",
                                    "type": 0
                                }
                            },
                            "bottomButtonInfo": {
                                "leftButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 1,
                                    "buttonHightColor": "#D80000,#E51C00,#E54615",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#F10000,#FF2000,#FF4F18",
                                    "buttonPriority": 240,
                                    "buttonText": "加入购物车",
                                    "buttonTextColor": "#ffffffff"
                                },
                                "rightButtonInfo": {
                                    "buttonBusiness": "businessCommonGoods",
                                    "buttonDisableColor": "#bfbfbf,#bfbfbf,#bfbfbf",
                                    "buttonEnable": true,
                                    "buttonEvent": 2,
                                    "buttonHightColor": "#E59500,#E59B00,#E6A800",
                                    "buttonNeedLogin": false,
                                    "buttonNormalColor": "#FFA600,#FFB000,#FFBC00",
                                    "buttonPriority": 240,
                                    "buttonSubText": "到手价¥1199",
                                    "buttonText": "立即购买",
                                    "buttonTextColor": "#ffffffff"
                                }
                            },
                            "pointInfo": {
                                "isShowAr": false,
                                "isOPType": 0
                            },
                            "wareImage": [
                                {
                                    "small": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg!q70.dpg.webp",
                                    "big": "https://m.360buyimg.com/mobilecms/s720x720_jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg!q70.dpg.webp",
                                    "share": "https://m.360buyimg.com/mobilecms/s120x120_jfs/t1/211433/25/25549/82008/6311cd8cEc2e95abc/4fb73ca95ed9b32a.jpg!q70.jpg"
                                }
                            ],
                            "oldMoreStyle": false,
                            "wareInfo": {
                                "name": "恒洁(HEGII)马桶×京东 国民家居 全包易清洁 节水防臭 虹吸式 大冲力连体坐便器HC0506PT（坑距305mm）",
                                "venderId": "1000105384",
                                "skuId": "100033963110"
                            },
                            "promotionInfo": {
                                "isBargain": false,
                                "prompt": ""
                            },
                            "tabUrl": "https://in.m.jd.com/product/graphext_0_0_0_0_0/100033963110.html",
                            "isOpenH5": true,
                            "isScf": false,
                            "locCouponFlag": false,
                            "generalTrackDic": {
                                "shopId": "1000105384",
                                "skuId": "100033963110"
                            },
                            "ddFatigueMechanism": {
                                "leftBottomBubbleCount": "5",
                                "leftBottomBubbleDuration": "5",
                                "ddBottomToastDuration": "10",
                                "ddBottomToastCount": "5",
                                "ddRightCount": "5"
                            },
                            "report": {
                                "darkIcon": "https://m.360buyimg.com/cc/jfs/t1/207978/19/1794/611/614c4aabE2b82675b/68307d0266c3aab7.png",
                                "icon": "https://m.360buyimg.com/cc/jfs/t1/199632/26/9352/400/614c4a83E7615a396/11f6a5f4c1eb8497.png",
                                "isOpen": true,
                                "jumpUrl": "https://h5.m.jd.com/babelDiy/Zeus/2WH8HhtMB5TQ2KcjAbrrW3atvo68/index.html#/home?pin=jd_61fdcba78e4b7&sku=100033963110",
                                "title": "我要反馈"
                            },
                            "appointAndPresaleOpen": true,
                            "plusShieldLandedPriceFlag": false,
                            "shareImgInfo": {
                                "priceDes": "价格具有时效性",
                                "promotionStr": "",
                                "markingOff": true,
                                "shareLanguage": "推荐一个好物给你，请查收",
                                "secondPrice": "1799.00",
                                "jprice": "1399.00",
                                "priceUrl": "https://m.360buyimg.com/cc/jfs/t1/32239/11/7019/5065/5c933b3dE4a485f97/f3a371d0407ef626.png"
                            }
                        },
                        "mId": "miniMasterdata",
                        "sortId": 1
                    },
                    {
                        "bId": "eCustom_flo_615",
                        "cf": {
                            "bgc": "#ffffff",
                            "spl": "empty"
                        },
                        "data": {
                            "preferentialGuide": {
                                "secondLevelGuideInfo": {
                                    "sLevelGuideText1": "使用以下“*”标优惠，预估价¥1199.00 ",
                                    "sLevelIconCode": "https://m.360buyimg.com/cc/jfs/t1/75639/12/11713/385/5d91bd73E9865f7db/4122771acdea2dc7.png",
                                    "sLevelGuideText2": "以上优惠仅为初步预估，不代表最终价格，规则可在帮助中心查看",
                                    "sLevelJumpUrl": "https://ihelp.jd.com/l/help/scene/getSceneDetail?id=322666",
                                    "saveMoney": "¥1199.00",
                                    "sLevelCode": "*"
                                },
                                "couponInfo": [
                                    {
                                        "addDays": 0,
                                        "anotherType": 0,
                                        "applicability": true,
                                        "area": 1,
                                        "batchId": 924197693,
                                        "beginHour": "20:00",
                                        "beginTime": "2022.09.06",
                                        "businessLabel": "0",
                                        "couponIcon": [],
                                        "couponId": "924197693",
                                        "couponKind": 1,
                                        "couponStyle": 0,
                                        "couponType": 1,
                                        "discount": 200,
                                        "discountText": "以下商品可使用满3000减200的优惠券",
                                        "encryptedKey": "DJ/nkkCwHhG4kRKEZvJmnj9ygzbEBq4z3W8ZfQcbLgo4CfEx2VF27w==",
                                        "endHour": "23:59",
                                        "endTime": "2022.09.10",
                                        "gwcCoupon": {
                                            "appid": "warecoresoa",
                                            "batchId": "924197693",
                                            "biinfo": "-100",
                                            "channel": "商品详情",
                                            "channelDetail": "-100",
                                            "couponPos": "可领券#0",
                                            "couponSource": "cmcGuide",
                                            "couponStatus": "0",
                                            "getType": 1,
                                            "platformid": "app-itemdetail",
                                            "skus": "100033963110",
                                            "subChannel": "优惠弹层"
                                        },
                                        "isMultipleDiscount": false,
                                        "isOverlap": false,
                                        "isPlusCoupon": false,
                                        "labelTxt": "限品类东券",
                                        "limitType": 5,
                                        "milliSecond": 0,
                                        "name": "仅可购买恒洁部分商品",
                                        "personalCoupon": false,
                                        "platform": 0,
                                        "quota": 3000,
                                        "roleId": 0,
                                        "roleIdCBC": "RWWe5zptEoXC1ThnMAfYaA==",
                                        "timeDesc": "有效期2022-09-06 20:00至2022-09-10 23:59",
                                        "toUrl": "www.jd.com,m.jd.com",
                                        "userRiskLevel": 0
                                    }
                                ],
                                "iconCode": "detail_var_045",
                                "couponIconText": "领券",
                                "firstLevelGuideInfo": {
                                    "fLevelGuideText": "使用优惠预估价¥1199.00 ",
                                    "saveMoney": "¥1199.00",
                                    "fLevelCode": "detail_047"
                                },
                                "businessEnum": "1",
                                "promotion": {
                                    "activity": [],
                                    "activityTypes": [
                                        "10",
                                        "15"
                                    ],
                                    "attach": [],
                                    "bestProList": [
                                        {
                                            "value": "每满699元，可减100元现金",
                                            "text": "满减",
                                            "skuId": "",
                                            "proSortNum": 40,
                                            "link": "",
                                            "proId": "200374253299",
                                            "promoId": "200374253299"
                                        }
                                    ],
                                    "canReturnHaggleInfo": false,
                                    "gift": [
                                        {
                                            "proId": "200375215811",
                                            "num": "1",
                                            "link": "",
                                            "text": "赠品",
                                            "value": "恒洁(HEGII)挂件卫生间纸巾盒全封闭防水纸巾盒厕纸架 太空铝厕纸盒HMP912-07（条件：购买1件及以上，赠完即止）",
                                            "skuId": "100023689966"
                                        }
                                    ],
                                    "isBargain": false,
                                    "isTwoLine": true,
                                    "limitBuyInfo": {
                                        "noSaleFlag": "0",
                                        "limitNum": "0",
                                        "strategyFlag": "0",
                                        "isPlusLimit": "0",
                                        "limitUserFlag": "1",
                                        "limitAreaFlag": "1",
                                        "limitPeopleFlag": "1"
                                    },
                                    "normalMark": "tab_var_071",
                                    "plusDiscountMap": {},
                                    "plusMark": "tab_var_124",
                                    "prompt": "",
                                    "screenLiPurMap": {},
                                    "tip": "",
                                    "tips": []
                                },
                                "hasFinanceCoupon": false
                            }
                        },
                        "mId": "miniAggrePromo",
                        "sortId": 2
                    }
                ],
                "others": {
                    "templateType": "mini"
                },
                "wait": 0
            },
            "jdShop": false
        },
    ]
}