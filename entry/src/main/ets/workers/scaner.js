import worker from '@ohos.worker';
import { LoadAndScanXml } from "../scan/xmlScaner"
import hilog from '@ohos.hilog';

var parentPort = worker.parentPort;

parentPort.onmessage = function (e) {
    let data = e.data;
    switch(data.type) {
        case "normal":
            hilog.fatal(0x0001, "testTag", "%{public}s World scaner.js parentPort.onmessage %{private}d", "hello", 3);
            let viewNode = LoadAndScanXml(data.data);
            //hilog.fatal(0x0001, "testTag", "%{public}s after convert %{private}d", JSON.stringify(viewNode).substring(0,10), 3);

            parentPort.postMessage({type: "ViewNodeUpdate", data: viewNode})
            break;
        default:
            break;
    }
}