import { ViewNode } from '../viewmodel/viewNode'
import convertxml from '@ohos.convertxml';
import fileio from '@ohos.fileio';
import hilog from '@ohos.hilog';

import { dsl_update_event } from '../test/base_test/case_jd'

export const UpdateAndScanXml = (url: string): ViewNode =>  {
    return dsl_update_event;
}

export const LoadAndScanXml = (url: string): ViewNode =>  {
    hilog.fatal(0x0001, "jddebug", "LoadAndScanXml===begin===");
    let xmlResult : any;
    try {
        let xmlInstance = new convertxml.ConvertXML();
        let dsl = fileio.readTextSync(url);
        dsl = dsl.replace(/\r\n/g, "");
            xmlResult = xmlInstance.convert(dsl, {
            trim: false,
            declarationKey: 'declaration',
            instructionKey: 'instruction',
            attributesKey: 'attributes',
            textKey: 'text',
            cdataKey: 'cdata',
            doctypeKey: 'doctype',
            commentKey: 'comment',
            parentKey: 'parent',
            typeKey: 'type',
            nameKey: 'viewName',
            elementsKey: 'children'
        });
        hilog.fatal(0x0001, "jddebug", "LoadAndScanXml===end===");
    } catch (e) {
        hilog.fatal(0x0001, "jddebug", "LoadAndScanXml exception===%{public}s", e);
    }

    return xmlResult;
}