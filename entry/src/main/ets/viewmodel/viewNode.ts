export const layoutId = 'layoutId';

export interface Attributes {
  [index: string]: string
}

export interface flag {
  [index: number]: boolean
}

export interface DynamicViewList {
  [index: number]: any
}

export interface Params {
  [index: string]: string
}

export interface ViewNodeList {
  [index: string]: ViewNode
}

export class ViewNode {
  readonly viewName: string;
  attributes?: Attributes;
  event?: any;
  children?: Array<ViewNode>;
  first?: { flag: boolean };
  update?: boolean;
  changer?: () => {};
  declaration?: Attributes;
}

export var viewNodeList: ViewNodeList = {}

export class RemoteEvent {
  readonly id: string;
  params?: Params;
}

export function Update(viewNode: ViewNode, event: any)
{
  if ((viewNode?.attributes) && (event?.params)) {
    viewNode.attributes["text"] =  event.params["text"]
    viewNode.attributes["fontSize"] =  event.params["fontSize"]
    viewNode.attributes["textColor"] =  event.params["textColor"]
    viewNode.attributes["width"] =  event.params["width"]
    viewNode.attributes["height"] =  event.params["height"]
    viewNode.attributes["backgroundColor"] =  event.params["backgroundColor"]
  }
}
